﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrepareManager : MonoBehaviour
{
    public Text StageText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.Instance.CurrentState == GameManager.State.Prepare)
        {
            StageText.gameObject.SetActive(true);
            StartCoroutine(PrepareCoroutine());
        }
        else
        {
            StageText.gameObject.SetActive(false);
        }
    }

    private IEnumerator PrepareCoroutine()
    {

        StageText.text = "Floor " + GameManager.Instance.CurrentStage;
        yield return new WaitForSeconds(1f);
        GameManager.Instance.Stageup();
        GameManager.Instance.ChangeState(GameManager.State.Card);
        yield return null;
    }
}
