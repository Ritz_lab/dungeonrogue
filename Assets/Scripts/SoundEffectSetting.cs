﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleState;

[RequireComponent(typeof(AudioSource))]
public class SoundEffectSetting : MonoBehaviour
{
    public AudioClip[] clips;
    private AudioSource source;

    public static SoundEffectSetting Instance;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySoundEffect(int num)
    {
        if(num > clips.Length - 1)
        {
            return;
        }
        source.PlayOneShot(clips[num]);
    }
}
