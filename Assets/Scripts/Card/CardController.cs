﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;

namespace CardState
{
    public class CardController : MonoBehaviour
    {
        public GameObject CardField;
        public Text CostText;
        public Button DecideButton;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (GameController.Instance.CurrentState == GameController.State.Card)
            {
                CardField.gameObject.SetActive(true);
                CostText.gameObject.SetActive(true);
                DecideButton.gameObject.SetActive(true);
            }
            else
            {
                CardField.gameObject.SetActive(false);
                CostText.gameObject.SetActive(false);
                DecideButton.gameObject.SetActive(false);
            }
        }
    }
}
