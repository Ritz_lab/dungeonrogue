﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;

namespace CardState
{
    public class CardCreator : MonoBehaviour
    {
        public static CardCreator Instance;
        public int Attack, Defense, HP, Event, Armor, Helmet, Boots, Weapon, Skill;
        public Text CostText;
        public Button DecideButton;
        public GameObject CardPrefab;
        public List<GameObject> CardPrefabs;
        public List<GameObject> SelectedCards;
        public Transform playerField;

        public int CardCount;
        public int MaxCardCount;

        public int MaxCost;
        public int CurrentCost;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this);
                return;
            }
            Instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            Reset();
        }

        // Update is called once per frame
        void Update()
        {
            CostText.text = CurrentCost.ToString() + " / " + MaxCost;
        }

        private void OnEnable()
        {
            if(CardCount >= MaxCardCount)
            {
                CardCount = MaxCardCount;
            }
            if(CardPrefabs.Count < CardCount)
            {
                for(int i = 0; i < CardCount - CardPrefabs.Count; i++)
                {
                    var obj = Instantiate(CardPrefab, playerField);
                    CardPrefabs.Add(obj);
                }
            }
            for (int i = 0; i < CardPrefabs.Count; i++)
            {
                CardPrefabs[i].SetActive(true);
            }
            CurrentCost = 0;
        }

        public void OnDecideCard()
        {
            for (int i = 0; i < CardPrefabs.Count; i++)
            {
                if (CardPrefabs[i].GetComponent<Card>().IsSelected)
                {
                    SelectedCards.Add(CardPrefabs[i]);
                }
            }
            GameController.Instance.ChangeState(GameController.State.BuildStart);
            Debug.Log("DecideButton Clicked");
        }

        public void Reset()
        {
            MaxCardCount = 18;
            CardCount = 9;
            MaxCost = 5;
            CurrentCost = 0;
            foreach (GameObject obj in CardPrefabs)
            {
                Destroy(obj);
            }
            CardPrefabs.RemoveAll(obj => CardPrefab);
            for (int i = 0; i < CardCount; i++)
            {
                var obj = Instantiate(CardPrefab, playerField);
                CardPrefabs.Add(obj);
            }
        }
    }
}