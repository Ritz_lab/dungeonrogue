﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CardState
{
    public class Card : MonoBehaviour
    {
        public Text CardText, CostText;
        public Button CardButton;
        public Image Frame, Icon;
        public ImageSetting ImageSetting;
        public float fadeTime = 1f;
        public float fadeDeltaTime = 0f;
        public enum State
        {
            Attack,
            Defense,
            HP,
            Speed,
            Event,
            Armor,
            Helmet,
            Boots,
            Gauntlet,
            Weapon,
            Skill
        }
        public State CurrentState
        {
            get; private set;
        }

        public int Cost
        {
            get; private set;
        }
        public bool IsSelected
        {
            get; private set;
        }
        private void Awake()
        {
            if (CardText == null)
            {
                CardText = GetComponent<Text>();
            }
            if (CostText == null)
            {
                CostText = GetComponentInChildren<Text>();
            }
            if (CardButton == null)
            {
                CardButton = GetComponent<Button>();
            }
            if (Frame == null)
            {
                Frame = GetComponentInChildren<Image>();
            }
        }
        // Start is called before the first frame update
        void Start()
        {
            Initialize();
            CardButton.onClick.AddListener(() => OnClick());
        }

        // Update is called once per frame
        void Update()
        {
            UpdateButton();
        }

        private void OnEnable()
        {
            Initialize();
        }

        private void OnDisable()
        {

        }

        private void Initialize()
        {
            IsSelected = false;
            CurrentState = SetState();
            BasicCardSetting(CurrentState);
            Frame.color = Color.black;
            Frame.fillOrigin = (int)Image.OriginVertical.Top;
            Frame.fillAmount = 1f;
        }



        private State SetCurrentState(int Attack, int Defense, int HP, int Event, int Armor, int Helmet, int Boots, int Weapon, int Skill, int num)
        {
            return CurrentState;
        }

        private void UpdateButton()
        {
            if (!IsSelected && CardCreator.Instance.CurrentCost + Cost > CardCreator.Instance.MaxCost)
            {
                CardButton.interactable = false;
            }
            else
            {
                CardButton.interactable = true;
            }
        }

        private void BasicCardSetting(State CurrentState)
        {
            switch (CurrentState)
            {
                case State.Attack:
                    Icon.sprite = ImageSetting.Attack;
                    Cost = 1;
                    break;
                case State.Defense:
                    Icon.sprite = ImageSetting.Defense;
                    Cost = 1;
                    break;
                case State.HP:
                    Icon.sprite = ImageSetting.HP;
                    Cost = 1;
                    break;
                case State.Speed:
                    Icon.sprite = ImageSetting.Speed;
                    Cost = 1;
                    break;
                case State.Helmet:
                    Icon.sprite = ImageSetting.Helmet;
                    Cost = 2;
                    break;
                case State.Armor:
                    Icon.sprite = ImageSetting.Armor;
                    Cost = 2;
                    break;
                case State.Gauntlet:
                    Icon.sprite = ImageSetting.Gauntlet;
                    Cost = 2;
                    break;
                case State.Boots:
                    Icon.sprite = ImageSetting.Boots;
                    Cost = 2;
                    break;
                case State.Weapon:
                    Icon.sprite = ImageSetting.Weapon;
                    Cost = 2;
                    break;
                case State.Skill:
                    Icon.sprite = ImageSetting.Skill;
                    Cost = 2;
                    break;
                default:
                    Cost = 1;
                    Icon.sprite = ImageSetting.Event;
                    break;
            }
            CardText.text = CurrentState.ToString();
            CostText.text = Cost.ToString();

        }

        private void OnClick()
        {
            if (IsSelected)
            {
                CardCreator.Instance.CurrentCost -= Cost;
                StartCoroutine(FadeOut(CardButton, Frame, Color.black, new Color(197f / 255f, 44f / 255f, 44f / 255f)));
            }
            else if (!IsSelected)
            {
                CardCreator.Instance.CurrentCost += Cost;
                StartCoroutine(FadeIn(CardButton, Frame, new Color(197f / 255f, 44f / 255f, 44f / 255f), Color.black));
            }
        }

        private bool Switch(bool Element)
        {
            //Debug.Log(Element + " " + CurrentState + this.name + " " + Cost);
            Element = Element ? false : true;
            Debug.Log(Element + " " + CurrentState + this.name + " " + Cost);
            return Element;
        }

        private State SetState()
        {
            Dictionary<State, int> StateDictionary = new Dictionary<State, int>(){
            {State.Attack,30 },
            {State.Defense,30 },
            {State.Speed,30 },
            {State.HP,30 },
            {State.Helmet,20 },
            {State.Armor,20 },
            {State.Gauntlet,20 },
            {State.Boots,20 },
            {State.Weapon,20 },
            {State.Skill,10 }
        };
            State nextState = Probability.DetermineFromDict<State>(StateDictionary);
            return nextState;
        }

        private IEnumerator FadeIn(Button button, Image image, Color prevColor, Color nextColor)
        {
            IsSelected = Switch(IsSelected);
            float fillAmount = 1f;
            fadeDeltaTime = 0f;
            image.fillOrigin = (int)Image.OriginVertical.Top;
            while (1f >= fadeDeltaTime)
            {
                fadeDeltaTime += Time.deltaTime / fadeTime;
                fillAmount -= Time.deltaTime / fadeTime;
                if (fillAmount < 0f)
                {
                    fillAmount = 0f;
                }
                image.color = Color.Lerp(nextColor, prevColor, fillAmount);
                image.fillAmount = fillAmount;
                button.interactable = false;
                yield return null;
            }
            button.interactable = true;
            yield return null;
        }

        private IEnumerator FadeOut(Button button, Image image, Color prevColor, Color nextColor)
        {
            IsSelected = Switch(IsSelected);
            float fillAmount = 0f;
            fadeDeltaTime = 0f;
            while (1f >= fadeDeltaTime)
            {
                fadeDeltaTime += Time.deltaTime / fadeTime;
                fillAmount += Time.deltaTime / fadeTime;
                if (fillAmount > 1f)
                {
                    fillAmount = 1f;
                }
                button.interactable = false;
                image.color = Color.Lerp(nextColor, prevColor, fillAmount);
                image.fillAmount = fillAmount;
                yield return null;
            }
            button.interactable = true;
            yield return null;
        }
    }
}
