﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    int up;
    int down;
    // Start is called before the first frame update
    void Start()
    {
        up = 0;
        //StartCoroutine(MYCoroutine(2));
        up = 0;
        StartCoroutine(anotherCoroutine(5));
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void checkupdate()
    {

    }

    public IEnumerator MYCoroutine(int num)
    {
        for (int i = 0; i < num; i++)
        {
            StartCoroutine(myCoroutine(i));
            yield return null;
        }
    }

    public IEnumerator myCoroutine(int i)
    {
        while (up < 10)
        {
            up++;
            Debug.Log("i =" + i + "/ up =" + up);
            yield return new WaitForSeconds(0.07f);
        }
    }
    public IEnumerator anotherCoroutine(int num)
    {
        for (int i = 0; i < num; i++)
        {
            StartCoroutine(ANOTHERCoroutine(i,10));
            yield return null;
        }
    }

    public IEnumerator ANOTHERCoroutine(int i,int num)
    {
        while(down > 0)
        {
            yield return null;
        }
        int temp = num;
        while (temp > 0)
        {
            up++;
            temp--;
            down = temp;
            Debug.Log("i =" + i + "/ up =" + up);
            yield return new WaitForSeconds(0.07f);
        }
    }

}
