﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using CardState;
using BattleState;
using Cysharp.Threading.Tasks;

namespace Manager
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance;

        public enum State
        {
            GameStart,
            Reset,
            StageUp,
            PrepareDungeon,
            PrepareElements,
            InDungeon,
            Pause,
            Card,
            BuildStart,
            CheckCard,
            PowerUp,
            GetEquipment,
            GetSkill,
            BuildEnd,
            Event,
            BattleStart,
            CheckTurn,
            CheckCondtions,
            PlayerTurn,
            EnemyTurn,
            BattleEnd,
            GameEnd,
            GameOver
        }

        public State CurrentState,NextState;

        public int KilledEnemy;

        public int CurrentStage;

        public int CardNumber;

        public Canvas BattleCanvas, CardCanvas,BuildCanvas,StartCanvas,EventCanvas,GameEndCanvas,GameOverCanvas,ControllerCanvas;


        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this);
                return;
            }
            Instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            ChangeState(State.GameStart);
        }

        // Update is called once per frame
        void Update()
        {
            UpdateState();

        }

        public void UpdateState()
        {
            switch (CurrentState)
            {
                case State.GameStart:
                    StartCanvas.gameObject.SetActive(true);
                    BattleCanvas.gameObject.SetActive(true);
                    BuildCanvas.gameObject.SetActive(true);
                    CardCanvas.gameObject.SetActive(true);
                    EventCanvas.gameObject.SetActive(true);
                    ControllerCanvas.gameObject.SetActive(true);
                    BattleCanvas.gameObject.SetActive(false);
                    BuildCanvas.gameObject.SetActive(false);
                    CardCanvas.gameObject.SetActive(false);
                    EventCanvas.gameObject.SetActive(false);
                    GameEndCanvas.gameObject.SetActive(false);
                    GameOverCanvas.gameObject.SetActive(false);
                    ControllerCanvas.gameObject.SetActive(false);
                    break;
                case State.Reset:
                    Player.Instance.Reset();
                    Enemy.Instance.Reset();
                    CardCreator.Instance.Reset();
                    ChangeState(State.StageUp);
                    DungeonManager.Instance.WallBreaker = 5;
                    Camera.main.orthographicSize = 5;
                    break;
                case State.StageUp:
                    ResetKilledEnemy();
                    StageUP();
                    CheckClear();
                    Player.Instance.AbnormalConditions["Burn"] = 0;
                    Player.Instance.AbnormalConditions["Addiction"] = 0;
                    Player.Instance.AbnormalConditions["Petrified"] = 0;
                    break;
                case State.InDungeon:
                    ControllerCanvas.gameObject.SetActive(true);
                    BattleCanvas.gameObject.SetActive(false);
                    BuildCanvas.gameObject.SetActive(false);
                    CardCanvas.gameObject.SetActive(false);
                    EventCanvas.gameObject.SetActive(false);
                    StartCanvas.gameObject.SetActive(false);
                    GameEndCanvas.gameObject.SetActive(false);
                    GameOverCanvas.gameObject.SetActive(false);
                    break;
                case State.Pause:
                    break;
                case State.Card:
                    CardCanvas.gameObject.SetActive(true);
                    ControllerCanvas.gameObject.SetActive(false);
                    break;
                case State.BuildStart:
                    BuildCanvas.gameObject.SetActive(true);
                    CardCanvas.gameObject.SetActive(false);
                    ChangeState(State.CheckCard);
                    break;
                case State.CheckCard:
                    if (CardCreator.Instance.SelectedCards != null)
                    {
                        if (CardCreator.Instance.SelectedCards.Count > 0)
                        {
                            SwitchBuildState(CardCreator.Instance.SelectedCards[CardNumber].GetComponent<Card>().CurrentState);
                        }
                        else
                        {
                            ChangeState(State.BuildEnd);
                        }
                    }
                    else
                    {
                        ChangeState(State.BuildEnd);
                    }
                    break;
                case State.BuildEnd:
                    BuildCanvas.gameObject.SetActive(false);
                    CardNumber = 0;
                    ChangeState(State.InDungeon);
                    break;
                case State.BattleStart:
                    ControllerCanvas.gameObject.SetActive(false);
                    BattleCanvas.gameObject.SetActive(true);
                    break;
                case State.CheckTurn:
                    InCheckTurn();
                    break;
                case State.CheckCondtions:
                    // await InCheckConditions(NextState);
                    break;
                case State.PlayerTurn:
                    break;
                case State.EnemyTurn:
                    break;
                case State.BattleEnd:
                    BattleCanvas.gameObject.SetActive(false);
                    InBattleEnd();
                    break;
                case State.Event:
                    EventCanvas.gameObject.SetActive(true);
                    ControllerCanvas.gameObject.SetActive(false);
                    break;
                case State.GameOver:
                    GameOverCanvas.gameObject.SetActive(true);
                    BattleCanvas.gameObject.SetActive(false);
                    BuildCanvas.gameObject.SetActive(false);
                    CardCanvas.gameObject.SetActive(false);
                    EventCanvas.gameObject.SetActive(false);
                    StartCanvas.gameObject.SetActive(false);
                    ControllerCanvas.gameObject.SetActive(false);
                    break;
                case State.GameEnd:
                    GameEndCanvas.gameObject.SetActive(true);
                    BattleCanvas.gameObject.SetActive(false);
                    BuildCanvas.gameObject.SetActive(false);
                    CardCanvas.gameObject.SetActive(false);
                    EventCanvas.gameObject.SetActive(false);
                    StartCanvas.gameObject.SetActive(false);
                    ControllerCanvas.gameObject.SetActive(false);
                    break;
            }
        }

        public void ChangeState(State state)
        {
            CurrentState = state;
        }

        public IEnumerator ChangeStateCoroutine(State state)
        {
            while(Player.Instance.IsWaitingCoroutine || Enemy.Instance.IsWaitingCoroutine)
            {
                yield return null;
            }
            ChangeState(state);
        }

        public void ResetKilledEnemy()
        {
            KilledEnemy = 0;
        }


        public void StartGame()
        {
            ChangeState(State.StageUp);
            StartCanvas.gameObject.SetActive(false);
        }
        public void SwitchBuildState(Card.State state)
        {
            if (CardCreator.Instance.SelectedCards == null)
            {
                Debug.Log("Switch");
                ChangeState(State.BuildEnd);
            }
            if(CardCreator.Instance.SelectedCards.Count <= 0)
            {
                Debug.Log("build");
                ChangeState(State.BuildEnd);
            }
            if (CurrentState == State.CheckCard)
            {
                switch (state)
                {
                    case Card.State.Attack:
                        ChangeState(State.PowerUp);
                        break;
                    case Card.State.Defense:
                        ChangeState(State.PowerUp);
                        break;
                    case Card.State.HP:
                        ChangeState(State.PowerUp);
                        break;
                    case Card.State.Speed:
                        ChangeState(State.PowerUp);
                        break;
                    case Card.State.Armor:
                        ChangeState(State.GetEquipment);
                        break;
                    case Card.State.Boots:
                        ChangeState(State.GetEquipment);
                        break;
                    case Card.State.Gauntlet:
                        ChangeState(State.GetEquipment);
                        break;
                    case Card.State.Helmet:
                        ChangeState(State.GetEquipment);
                        break;
                    case Card.State.Weapon:
                        ChangeState(State.GetEquipment);
                        break;
                    case Card.State.Skill:
                        ChangeState(State.GetSkill);
                        break;
                }
            }
            Debug.Log(state);
        }

        public void CheckPauseInput()
        {
            if(CurrentState == State.InDungeon)
            {
                ChangeState(State.Pause);
                Camera.main.orthographicSize += 10;
            }
            else if(CurrentState == State.Pause)
            {
                ChangeState(State.InDungeon);
                Camera.main.orthographicSize -= 10;
            }
        }

        public void CheckClear()
        {
            if(CurrentStage > 10)
            {
                ChangeState(State.GameEnd);
            }
            else
            {
                ChangeState(State.PrepareDungeon);
            }
        }

        public void InCheckTurn()
        {
            Debug.Log(CurrentState);
            for (int i = 0; i < 5; i++)
            {
                Enemy.Instance.Skills[i].GetComponent<Skill>().button.interactable = false;
                Player.Instance.Skills[i].GetComponent<Skill>().button.interactable = false;
            }
            if (Player.Instance.CurrentHP <= 0 || Enemy.Instance.CurrentHP <= 0 || Player.Instance.AbnormalConditions["Petrified"] >= 100 || Enemy.Instance.AbnormalConditions["Petrified"] >= 100)
            {
                ChangeState(State.BattleEnd);
                return;
            }
            if(Player.Instance.IsTurnEnd && Enemy.Instance.IsTurnEnd)
            {
                Player.Instance.IsTurnEnd = false;
                Enemy.Instance.IsTurnEnd = false;
            }
            if(!Player.Instance.IsTurnEnd && !Enemy.Instance.IsTurnEnd)
            {

                if(Player.Instance.Speed >= Enemy.Instance.Speed)
                {
                    Player.Instance.IsCheckedConditions = false;
                    NextState = State.PlayerTurn;
                    ChangeState(State.CheckCondtions);
                }
                else
                {
                    Enemy.Instance.IsCheckedConditions = false;
                    NextState = State.EnemyTurn;
                    ChangeState(State.CheckCondtions);
                }
            }
            else if(!Player.Instance.IsTurnEnd && Enemy.Instance.IsTurnEnd)
            {
                Player.Instance.IsCheckedConditions = false;
                NextState = State.PlayerTurn;
                ChangeState(State.CheckCondtions);
            }
            else if(Player.Instance.IsTurnEnd && !Enemy.Instance.IsTurnEnd)
            {
                Enemy.Instance.IsCheckedConditions = false;
                NextState = State.EnemyTurn;
                ChangeState(State.CheckCondtions);
            }
            Debug.Log("CheckTurn");
        }

        public async UniTask InCheckConditions(State state)
        {
            if (CurrentState == State.CheckCondtions)
            {
                Debug.Log("CurrentState=" + CurrentState + "/nextState=" + state);
                if (state == State.EnemyTurn)
                {
                    if (!Enemy.Instance.IsCheckedConditions)
                    {
                        Enemy.Instance.IsCheckedConditions = true;
                        await Enemy.Instance.CheckAddiction();
                        await Enemy.Instance.CheckBurn();
                        if (Enemy.Instance.CurrentHP > 0 && Player.Instance.CurrentHP > 0)
                        {
                            await UniTask.Delay(500);
                            ChangeState(state);
                        }
                        else
                        {
                            await UniTask.Delay(1000);
                            ChangeState(State.BattleEnd);
                        }
                    }
                }
                Debug.Log("CheckCondition End");
            }
        }

        public void InBattleEnd()
        {
            if (Player.Instance.CurrentHP == 0 || Player.Instance.AbnormalConditions["Petrified"] >= 100)
            {
                ChangeState(State.GameOver);
            }
            else
            {
                if (Enemy.Instance.IsBoss)
                {
                    DungeonManager.Instance.WallBreaker = 5;
                    ChangeState(State.StageUp);
                }
                else
                {
                    DungeonManager.Instance.WallBreaker++;
                    KilledEnemy++;
                    int rand = Random.Range(0, 4);
                    if (rand == 1)
                    {
                        ChangeState(State.Card);
                    }
                    else
                    {
                        ChangeState(State.InDungeon);
                    }
                    Debug.Log("InBattleEnd");
                }
            }
            foreach(RectTransform obj in Player.Instance.DamageTextField)
            {
                obj.gameObject.SetActive(false);
            }
            foreach (RectTransform obj in Enemy.Instance.DamageTextField)
            {
                obj.gameObject.SetActive(false);
            }
        }
        public void PlusCardNumber()
        {
            if (CardNumber < CardCreator.Instance.SelectedCards.Count)
            {
                CardNumber++;
            }
        }
        public void StageUP()
        {
            CurrentStage++;
        }


    }
}
