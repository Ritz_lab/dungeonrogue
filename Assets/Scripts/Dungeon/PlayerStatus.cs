﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BattleState;
namespace Manager {
    public class PlayerStatus : MonoBehaviour
    {
        public Image PlayerHPImage;

        public Text CurrentHPText;

        public Text StatusText;

        public Button ZoomButton,RestartButton;

        // Start is called before the first frame update
        void Start()
        {
            ZoomButton.onClick.AddListener(() => GameController.Instance.CheckPauseInput());
        }

        // Update is called once per frame
        void Update()
        {
            if (GameController.Instance.CurrentState == GameController.State.InDungeon)
            {
            
                PlayerHPImage.fillAmount = (float)Player.Instance.CurrentHP / (float)Player.Instance.MaxHP;
                Debug.Log(Player.Instance.CurrentHP / Player.Instance.MaxHP);
                if (Player.Instance.CurrentHP > Player.Instance.MaxHP * 0.5f)
                {
                    PlayerHPImage.color = Color.green;
                }
                else if (Player.Instance.CurrentHP <= Player.Instance.MaxHP * 0.5f && Player.Instance.CurrentHP > Player.Instance.MaxHP * 0.15f)
                {
                    PlayerHPImage.color = Color.yellow;
                }
                else
                {
                    PlayerHPImage.color = Color.red;
                }
                CurrentHPText.text = Player.Instance.CurrentHP.ToString();
                StatusText.text = "Attack : " + Player.Instance.Attack + "\nDefense : " + Player.Instance.Defense + "\nSpeed : " + Player.Instance.Speed + "\nFloor : " + GameController.Instance.CurrentStage;
                RestartButton.gameObject.SetActive(false);
            }
            else if(GameController.Instance.CurrentState == GameController.State.Pause)
            {
                RestartButton.gameObject.SetActive(true);
            }
        }
    }
}
