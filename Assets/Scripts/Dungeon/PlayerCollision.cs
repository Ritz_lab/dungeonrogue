﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manager;
using BattleState;
public class PlayerCollision : MonoBehaviour
{
    private void Awake()
    {

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "EnemyInDungeon")
        {
            Enemy.Instance.IsBoss = false;
            GameController.Instance.ChangeState(GameController.State.BattleStart);
            collision.gameObject.SetActive(false);
        }
        else if(collision.gameObject.tag == "Boss")
        {
            Enemy.Instance.IsBoss = true;
            GameController.Instance.ChangeState(GameController.State.BattleStart);
            collision.gameObject.SetActive(false);
        }
        else if(collision.gameObject.tag == "Event")
        {
            GameController.Instance.ChangeState(GameController.State.Event);
            collision.gameObject.SetActive(false);
        }
        else if(collision.gameObject.tag == "Build")
        {
            GameController.Instance.ChangeState(GameController.State.Card);
            collision.gameObject.SetActive(false);
        }
    }
}
