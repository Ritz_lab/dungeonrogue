﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manager;

namespace InDungeon
{
    public class PlayerInDungeon : MonoBehaviour
    {
        public static PlayerInDungeon Instance;

        private void Awake()
        {
            if(Instance != null)
            {
                Destroy(this);
                return;
            }
            Instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }


    }
}
