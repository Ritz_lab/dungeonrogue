﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BattleState;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

namespace Manager
{
    public class DungeonManager : MonoBehaviour
    {
        public static DungeonManager Instance;

        public int MapMinWidth = 50;
        public int MapMaxWidth = 70;
        public int MapMinHeight = 50;
        public int MapMaxHeight = 70;
        public int MapWidth = 50;
        public int MapHeight = 50;

        public int[,] Map;
        public int[,] SimpleMap;

        public const int road = 0;
        public const int player = 1;
        public const int enemy = 2;
        public const int boss = 3;
        public const int buildmass = 4;
        public const int eventmass = 5;
        public const int wall = 9;


        public int WallBreaker;
        public GameObject WallObject;

        public GameObject RoadObject;

        public GameObject PlayerPrefab;

        public GameObject Player;

        public GameObject EnemyPrefab;

        public GameObject BossPrefab;

        public GameObject Boss;

        public GameObject BuildMass;

        public GameObject EventMass;

        public List<GameObject> Enemies;

        public List<int> EnemyDirections;

        public Tilemap DungeonMap;

        public TileBase WallRight, WallLeft, WallUp, WallDown, WallUpRight, WallDownRight, WallUpLeft, WallDownLeft, WallUpRightIn, WallDownRightIn, WallUpLeftIn, WallDownLeftIn,Wall,WallFlat;

        public TileBase Road1, Road2, Road3, Road4 ,Road5;

        public Stick PlayerStick;

        public Text WallBreakerText;

        public int player_v, player_h;

        const int roomMinHeight = 5;
        const int roomMaxHeight = 10;

        const int roomMinWidth = 3;
        const int roomMaxWidth = 7;

        const int RoomCountMin = 15;
        const int RoomCountMax = 20;

        //道の集合点を増やしたいならこれを増やす
        const int meetPointCount = 7;

        bool IsPlayerInstantiated;

        bool IsBossInstantiated;

        float MoveSpeed = 0.01f;


        public bool IsDungeonCreated;

        private void Awake()
        {
            if(Instance != null)
            {
                Destroy(this);
                return;
            }
            Instance = this;
        }

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (GameController.Instance.CurrentState == GameController.State.PrepareDungeon)
            {
                IsPlayerInstantiated = false;
                IsBossInstantiated = false;
                if(Player != null)
                {
                    Destroy(Player);
                }
                if(Boss != null)
                {
                    Destroy(Boss);
                }
                ResetMapData();

                CreateSpaceData();

                CreateDungeon();
                GameController.Instance.ChangeState(GameController.State.PrepareElements);
            }
            else if(GameController.Instance.CurrentState == GameController.State.PrepareElements)
            {
                PrepareElements();
                GameController.Instance.ChangeState(GameController.State.InDungeon);
            }
            else if(GameController.Instance.CurrentState == GameController.State.InDungeon)
            {
                //CheckPlayerInput();
                if (Input.GetMouseButtonDown(0) && !PlayerStick.IsPush)
                {
                    Vector3 mousePosition = Input.mousePosition;
                    Vector3 screenToWorldPoint = Camera.main.ScreenToWorldPoint(mousePosition);
                    WallBreak(screenToWorldPoint);
                }
                WallBreakerText.text = "WallBreak : " + WallBreaker;
            }

        }


        private void DestroyObjects(GameObject[,] objects)
        {
            foreach (GameObject obj in objects)
            {
                Destroy(obj);
            }
        }
        /// <summary>
        /// Mapの二次元配列の初期化
        /// </summary>
        private void ResetMapData()
        {
            Map = new int[MapHeight, MapWidth];
            SimpleMap = new int[MapHeight, MapWidth];
            for (int i = 0; i < MapHeight; i++)
            {
                for (int j = 0; j < MapWidth; j++)
                {
                    Map[i, j] = wall;
                    SimpleMap[i, j] = wall;
                }
            }
            foreach(GameObject obj in Enemies)
            {
                Destroy(obj);
            }
            GameObject[] events = GameObject.FindGameObjectsWithTag("Event");
            foreach(GameObject obj in events)
            {
                Destroy(obj);
            }
            GameObject[] builds = GameObject.FindGameObjectsWithTag("Build");
            foreach (GameObject obj in builds)
            {
                Destroy(obj);
            }
            Enemies.Clear();
            EnemyDirections.Clear();
        }

        /// <summary>
        /// 空白部分のデータを変更
        /// </summary>
        private void CreateSpaceData()
        {
            int roomCount = Random.Range(RoomCountMin, RoomCountMax);

            int[] meetPointsX = new int[meetPointCount];
            int[] meetPointsY = new int[meetPointCount];
            for (int i = 0; i < meetPointsX.Length; i++)
            {
                meetPointsX[i] = Random.Range(MapWidth / 4, MapWidth * 3 / 4);
                meetPointsY[i] = Random.Range(MapHeight / 4, MapHeight * 3 / 4);
                Map[meetPointsY[i], meetPointsX[i]] = road;
                SimpleMap[meetPointsY[i], meetPointsX[i]] = road;
            }

            for (int i = 0; i < roomCount; i++)
            {
                int roomHeight = Random.Range(roomMinHeight, roomMaxHeight);
                int roomWidth = Random.Range(roomMinWidth, roomMaxWidth);
                int roomPointX = Random.Range(2, MapWidth - roomMaxWidth - 2);
                int roomPointY = Random.Range(2, MapWidth - roomMaxWidth - 2);

                int roadStartPointX = Random.Range(roomPointX, roomPointX + roomWidth);
                int roadStartPointY = Random.Range(roomPointY, roomPointY + roomHeight);

                bool isRoad = CreateRoomData(roomHeight, roomWidth, roomPointX, roomPointY);

                if (isRoad == false)
                {
                    CreateRoadData(roadStartPointX, roadStartPointY, meetPointsX[Random.Range(0, 0)], meetPointsY[Random.Range(0, 0)]);
                }
            }


        }

        /// <summary>
        /// 部屋データを生成。すでに部屋がある場合はtrueを返し、道を作らないようにする
        /// </summary>
        /// <param name="roomHeight">部屋の高さ</param>
        /// <param name="roomWidth">部屋の横幅</param>
        /// <param name="roomPointX">部屋の始点(x)</param>
        /// <param name="roomPointY">部屋の始点(y)</param>
        /// <returns></returns>
        private bool CreateRoomData(int roomHeight, int roomWidth, int roomPointX, int roomPointY)
        {
            bool isRoad = false;
            for (int i = 0; i < roomHeight; i++)
            {
                for (int j = 0; j < roomWidth; j++)
                {
                    if (Map[roomPointY + i, roomPointX + j] == road)
                    {
                        isRoad = true;
                    }
                    else
                    {
                        Map[roomPointY + i, roomPointX + j] = SetMass();
                        SimpleMap[roomPointY + i, roomPointX + j] = road;
                    }
                }
            }
            return isRoad;
        }

        /// <summary>
        /// 道データを生成
        /// </summary>
        /// <param name="roadStartPointX"></param>
        /// <param name="roadStartPointY"></param>
        /// <param name="meetPointX"></param>
        /// <param name="meetPointY"></param>
        private void CreateRoadData(int roadStartPointX, int roadStartPointY, int meetPointX, int meetPointY)
        {

            bool isRight;
            if (roadStartPointX > meetPointX)
            {
                isRight = true;
            }
            else
            {
                isRight = false;
            }
            bool isUnder;
            if (roadStartPointY > meetPointY)
            {
                isUnder = false;
            }
            else
            {
                isUnder = true;
            }

            if (Random.Range(0, 2) == 0)
            {

                while (roadStartPointX != meetPointX)
                {

                    Map[roadStartPointY, roadStartPointX] = road;
                    SimpleMap[roadStartPointY, roadStartPointX] = road;
                    if (isRight == true)
                    {
                        roadStartPointX--;
                    }
                    else
                    {
                        roadStartPointX++;
                    }

                }

                while (roadStartPointY != meetPointY)
                {

                    Map[roadStartPointY, roadStartPointX] = road;
                    SimpleMap[roadStartPointY, roadStartPointX] = road;
                    if (isUnder == true)
                    {
                        roadStartPointY++;
                    }
                    else
                    {
                        roadStartPointY--;
                    }
                }

            }
            else
            {

                while (roadStartPointY != meetPointY)
                {

                    Map[roadStartPointY, roadStartPointX] = road;
                    SimpleMap[roadStartPointY, roadStartPointX] = road;
                    if (isUnder == true)
                    {
                        roadStartPointY++;
                    }
                    else
                    {
                        roadStartPointY--;
                    }
                }

                while (roadStartPointX != meetPointX)
                {

                    Map[roadStartPointY, roadStartPointX] = road;
                    SimpleMap[roadStartPointY, roadStartPointX] = road;
                    if (isRight == true)
                    {
                        roadStartPointX--;
                    }
                    else
                    {
                        roadStartPointX++;
                    }

                }

            }
        }

        private int SetMass()
        {
            Dictionary<int, int> MassDictionary = new Dictionary<int, int>
            {
                {road,80 },
                {enemy,5 },
                {buildmass,7 },
                {eventmass,3 }
            };
            int nextMass = Probability.DetermineFromDict<int>(MassDictionary);
            return nextMass;
        }

        /// <summary>
        /// マップデータをもとにダンジョンを生成
        /// </summary>
        private void CreateDungeon()
        {
            for (int i = 0; i < MapHeight; i++)
            {
                for (int j = 0; j < MapWidth; j++)
                {
                    if (SimpleMap[i, j] == wall)
                    {
                        DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), Wall);
                        if(0 < i && i < MapHeight - 1 && 0 < j && j < MapWidth - 1)
                        {
                            if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallRight);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallLeft);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallUp);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallDown);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallDownRight);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallDownLeft);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallUpRight);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallUpLeft);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallDownRight);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallDownLeft);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallUpRight);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallUpLeft);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == wall && SimpleMap[i+1,j+1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallUpRightIn);
                            } 
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == wall && SimpleMap[i+1,j-1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallUpLeftIn);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == wall && SimpleMap[i-1,j+1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallDownRightIn);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == wall && SimpleMap[i-1,j-1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallDownLeftIn);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallFlat);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallFlat);
                            }
                            else if (SimpleMap[i + 1, j] == wall && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallFlat);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == wall && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallFlat);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == wall && SimpleMap[i, j - 1] == road)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallFlat);
                            }
                            else if (SimpleMap[i + 1, j] == road && SimpleMap[i - 1, j] == road && SimpleMap[i, j + 1] == road && SimpleMap[i, j - 1] == wall)
                            {
                                DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), WallFlat);
                            }
                        }
                    }
                    else if(SimpleMap[i,j] == road)
                    {
                        DungeonMap.SetTile(new Vector3Int(j - MapWidth / 2, i - MapHeight / 2, 0), Road1);
                    }

                }
            }
            SpawnPlayer();
            SpawnBoss();
        }

        private void SpawnPlayer()
        {
            if (!IsPlayerInstantiated)
            {
                while (!IsPlayerInstantiated)
                {
                    int i = Random.Range(2, MapWidth - 2);
                    int j = Random.Range(2, MapHeight - 2);
                    if (Map[i, j] == road && Map[i + 1, j] == road && Map[i - 1, j] == road && Map[i, j + 1] == road && Map[i, j - 1] == road)
                    {
                        player_v = i;
                        player_h = j;
                        Map[player_v, player_h] = player;
                        IsPlayerInstantiated = true;
                    }
                }
            }
        }

        private void SpawnBoss()
        {
            while (!IsBossInstantiated)
            {
                int i = Random.Range(2, MapWidth - 2);
                int j = Random.Range(2, MapHeight - 2);
                if (Map[i, j] == road && Map[i + 1, j] == road && Map[i - 1, j] == road && Map[i, j + 1] == road && Map[i, j - 1] == road)
                {
                    Map[i, j] = boss;
                    IsBossInstantiated = true;
                }
            }
        }

        public void PrepareElements()
        {
            Enemies = new List<GameObject>();
            EnemyDirections = new List<int>();
            for (int i = 0; i < MapHeight; i++)
            {
                for (int j = 0; j < MapWidth; j++)
                {
                    if (Map[i, j] == player)
                    {
                        Player = Instantiate(PlayerPrefab);
                        Player.transform.position = new Vector3(j - MapWidth / 2 + 0.5f, i - MapHeight / 2 + 0.5f, 0);
                    }
                    else if (Map[i, j] == enemy)
                    {
                        var obj = Instantiate(EnemyPrefab);
                        obj.transform.position = new Vector3(j - MapWidth / 2 + 0.5f, i - MapHeight / 2 + 0.5f, 0);
                        Enemies.Add(obj);
                    }
                    else if (Map[i, j] == boss)
                    {
                        Boss = Instantiate(BossPrefab);
                        Boss.transform.position = new Vector3(j - MapWidth / 2 + 0.5f, i - MapHeight / 2 + 0.5f, 0);
                    }
                    else if (Map[i, j] == buildmass)
                    {
                        var obj = Instantiate(BuildMass);
                        obj.transform.position = new Vector3(j - MapWidth / 2 + 0.5f, i - MapHeight / 2 + 0.5f, 0);
                    }
                    else if (Instance.Map[i, j] == eventmass)
                    {
                        var obj = Instantiate(EventMass);
                        obj.transform.position = new Vector3(j - MapWidth / 2 + 0.5f, i - MapHeight / 2 + 0.5f, 0);
                    }
                }
            }
        }

        public void CheckPlayerInput()
        {
            if (PlayerStick.IsPush)
            {
                for(int i = 0; i < Enemies.Count; i++)
                {
                    int rand = Random.Range(0, 100);
                    if(rand == 0)
                    {

                    }
                    switch (EnemyDirections[i])
                    {
                        case 0:
                            Enemies[i].transform.position = new Vector3(Enemies[i].transform.position.x + MoveSpeed, Enemies[i].transform.position.y, 0);
                            break;
                        case 1:
                            Enemies[i].transform.position = new Vector3(Enemies[i].transform.position.x - MoveSpeed, Enemies[i].transform.position.y, 0);
                            break;
                        case 2:
                            Enemies[i].transform.position = new Vector3(Enemies[i].transform.position.x, Enemies[i].transform.position.y + MoveSpeed, 0);
                            break;
                        case 3:
                            Enemies[i].transform.position = new Vector3(Enemies[i].transform.position.x, Enemies[i].transform.position.y - MoveSpeed, 0);
                            break;
                    }
                }
            }
        }

        public void WallBreak(Vector3 pos)
        {
            if (WallBreaker > 0)
            {
                Vector3Int clickedpos = DungeonMap.WorldToCell(pos);
                if (DungeonMap.GetTile(clickedpos) != Road1)
                {
                    DungeonMap.SetTile(clickedpos, Road1);
                    WallBreaker--;
                }
            }
            
        }
    }
}