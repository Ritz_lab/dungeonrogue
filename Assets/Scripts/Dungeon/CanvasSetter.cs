﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasSetter : MonoBehaviour
{
    public Canvas canvas;

    private void Awake()
    {
        if(canvas == null)
        {
            canvas = GetComponentInChildren<Canvas>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        canvas.worldCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
