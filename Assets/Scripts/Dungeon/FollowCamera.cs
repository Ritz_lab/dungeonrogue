﻿using UnityEngine;
using Manager;

[RequireComponent(typeof(Camera))]
public class FollowCamera : MonoBehaviour
{
    public GameObject player;
    private void Awake()
    {

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.Instance.CurrentState == GameController.State.InDungeon)
        {
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("Player");
            }
        }
    }

    private void LateUpdate()
    {
        if (GameController.Instance.CurrentState == GameController.State.InDungeon && player != null)
        {
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
        }

    }
}
