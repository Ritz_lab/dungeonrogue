﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BattleState;

namespace Manager
{
    public class DungeonSystemManager : MonoBehaviour
    {
        public static DungeonSystemManager Instance;
        static int MapWidth = 50;
        static int MapHeight = 50;

        public int[,] Map;

        const int road = 0;
        const int player = 1;
        const int enemy = 2;
        const int boss = 3;
        const int buildmass = 4;
        const int eventmass = 5;
        const int wall = 9;

        public GameObject WallObject;

        public GameObject RoadObject;

        public GameObject PlayerPrefab;

        public GameObject Player;

        public GameObject EnemyPrefab;

        public GameObject BossPrefab;

        public GameObject Boss;

        public GameObject BuildMass;

        public GameObject EventMass;

        public GameObject[,] DungeonElements;

        public GameObject[,] RoadObjects;

        public int player_v, player_h;

        const int roomMinHeight = 5;
        const int roomMaxHeight = 10;

        const int roomMinWidth = 5;
        const int roomMaxWidth = 7;

        const int RoomCountMin = 10;
        const int RoomCountMax = 17;

        //道の集合点を増やしたいならこれを増やす
        const int meetPointCount = 3;

        bool IsPlayerInstantiated;

        bool IsBossInstantiated;

        public bool IsDungeonCreated;

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (GameController.Instance.CurrentState == GameController.State.PrepareDungeon)
            {
                IsPlayerInstantiated = false;
                IsBossInstantiated = false;
                ResetMapData();

                CreateSpaceData();

                CreateDungeon();
                GameController.Instance.ChangeState(GameController.State.InDungeon);
            }
            else if (GameController.Instance.CurrentState == GameController.State.InDungeon)
            {
                CheckInput();
            }
        }

        private void LateUpdate()
        {
            
        }


        private void DestroyObjects(GameObject[,] objects)
        {
            foreach(GameObject obj in objects)
            {
                Destroy(obj);
            }
        }
        /// <summary>
        /// Mapの二次元配列の初期化
        /// </summary>
        private void ResetMapData()
        {
            if(DungeonElements != null)
            {
                DestroyObjects(DungeonElements);
            }
            if(RoadObjects != null)
            {
                DestroyObjects(RoadObjects);
            }
            RoadObjects = new GameObject[MapHeight, MapWidth];
            DungeonElements = new GameObject[MapHeight, MapWidth];
            Map = new int[MapHeight, MapWidth];
            for (int i = 0; i < MapHeight; i++)
            {
                for (int j = 0; j < MapWidth; j++)
                {
                    Map[i, j] = wall;
                }
            }
        }

        /// <summary>
        /// 空白部分のデータを変更
        /// </summary>
        private void CreateSpaceData()
        {
            int roomCount = Random.Range(RoomCountMin, RoomCountMax);

            int[] meetPointsX = new int[meetPointCount];
            int[] meetPointsY = new int[meetPointCount];
            for (int i = 0; i < meetPointsX.Length; i++)
            {
                meetPointsX[i] = Random.Range(MapWidth / 4, MapWidth * 3 / 4);
                meetPointsY[i] = Random.Range(MapHeight / 4, MapHeight * 3 / 4);
                Map[meetPointsY[i], meetPointsX[i]] = road;
            }

            for (int i = 0; i < roomCount; i++)
            {
                int roomHeight = Random.Range(roomMinHeight, roomMaxHeight);
                int roomWidth = Random.Range(roomMinWidth, roomMaxWidth);
                int roomPointX = Random.Range(2, MapWidth - roomMaxWidth - 2);
                int roomPointY = Random.Range(2, MapWidth - roomMaxWidth - 2);

                int roadStartPointX = Random.Range(roomPointX, roomPointX + roomWidth);
                int roadStartPointY = Random.Range(roomPointY, roomPointY + roomHeight);

                bool isRoad = CreateRoomData(roomHeight, roomWidth, roomPointX, roomPointY);

                if (isRoad == false)
                {
                    CreateRoadData(roadStartPointX, roadStartPointY, meetPointsX[Random.Range(0, 0)], meetPointsY[Random.Range(0, 0)]);
                }
            }


        }

        /// <summary>
        /// 部屋データを生成。すでに部屋がある場合はtrueを返し、道を作らないようにする
        /// </summary>
        /// <param name="roomHeight">部屋の高さ</param>
        /// <param name="roomWidth">部屋の横幅</param>
        /// <param name="roomPointX">部屋の始点(x)</param>
        /// <param name="roomPointY">部屋の始点(y)</param>
        /// <returns></returns>
        private bool CreateRoomData(int roomHeight, int roomWidth, int roomPointX, int roomPointY)
        {
            bool isRoad = false;
            for (int i = 0; i < roomHeight; i++)
            {
                for (int j = 0; j < roomWidth; j++)
                {
                    if (Map[roomPointY + i, roomPointX + j] == road)
                    {
                        isRoad = true;
                    }
                    else
                    {
                        Map[roomPointY + i, roomPointX + j] = SetMass();
                    }
                }
            }
            return isRoad;
        }

        /// <summary>
        /// 道データを生成
        /// </summary>
        /// <param name="roadStartPointX"></param>
        /// <param name="roadStartPointY"></param>
        /// <param name="meetPointX"></param>
        /// <param name="meetPointY"></param>
        private void CreateRoadData(int roadStartPointX, int roadStartPointY, int meetPointX, int meetPointY)
        {

            bool isRight;
            if (roadStartPointX > meetPointX)
            {
                isRight = true;
            }
            else
            {
                isRight = false;
            }
            bool isUnder;
            if (roadStartPointY > meetPointY)
            {
                isUnder = false;
            }
            else
            {
                isUnder = true;
            }

            if (Random.Range(0, 2) == 0)
            {

                while (roadStartPointX != meetPointX)
                {

                    Map[roadStartPointY, roadStartPointX] = road;
                    if (isRight == true)
                    {
                        roadStartPointX--;
                    }
                    else
                    {
                        roadStartPointX++;
                    }

                }

                while (roadStartPointY != meetPointY)
                {

                    Map[roadStartPointY, roadStartPointX] = road;
                    if (isUnder == true)
                    {
                        roadStartPointY++;
                    }
                    else
                    {
                        roadStartPointY--;
                    }
                }

            }
            else
            {

                while (roadStartPointY != meetPointY)
                {

                    Map[roadStartPointY, roadStartPointX] = road;
                    if (isUnder == true)
                    {
                        roadStartPointY++;
                    }
                    else
                    {
                        roadStartPointY--;
                    }
                }

                while (roadStartPointX != meetPointX)
                {

                    Map[roadStartPointY, roadStartPointX] = road;
                    if (isRight == true)
                    {
                        roadStartPointX--;
                    }
                    else
                    {
                        roadStartPointX++;
                    }

                }

            }
        }

        private int SetMass()
        {
            Dictionary<int, int> MassDictionary = new Dictionary<int, int>
            {
                {road,80 },
                {enemy,10 },
                {buildmass,3 },
                {eventmass,2 }
            };
            int nextMass = Probability.DetermineFromDict<int>(MassDictionary);
            return nextMass;
        }

        /// <summary>
        /// マップデータをもとにダンジョンを生成
        /// </summary>
        private void CreateDungeon()
        {
            for (int i = 0; i < MapHeight; i++)
            {
                for (int j = 0; j < MapWidth; j++)
                {
                    if (Map[i, j] == wall)
                    {
                        DungeonElements[i,j] = Instantiate(WallObject, new Vector3(j - MapWidth / 2, i - MapHeight / 2, 0), Quaternion.identity);
                    }

                    else if(Map[i,j] == enemy)
                    {
                        DungeonElements[i, j] = Instantiate(EnemyPrefab, new Vector3(j - MapWidth / 2, i - MapHeight / 2, 0), Quaternion.identity);
                    }
                    else if(Map[i,j] == buildmass)
                    {
                        DungeonElements[i, j] = Instantiate(BuildMass, new Vector3(j - MapWidth / 2, i - MapHeight / 2, 0), Quaternion.identity);
                    }
                    else if(Map[i,j] == eventmass)
                    {
                        DungeonElements[i, j] = Instantiate(EventMass, new Vector3(j - MapWidth / 2, i - MapHeight / 2, 0), Quaternion.identity);
                    }

                    if (Map[i, j] != wall)
                    {
                        RoadObjects[i,j] = Instantiate(RoadObject, new Vector3(j - MapWidth / 2, i - MapHeight / 2, 0), Quaternion.identity);
                    }

                }
            }
            SpawnPlayer();
            SpawnBoss();
        }

        private void SpawnPlayer()
        {
            if (!IsPlayerInstantiated)
            {
                while (!IsPlayerInstantiated)
                {
                    int i = Random.Range(2,MapWidth-2);
                    int j = Random.Range(2, MapHeight-2);
                    if (Map[i, j] == road && Map[i + 1, j] == road && Map[i - 1, j] == road && Map[i,j+1] == road && Map[i,j-1] == road)
                    {
                        DungeonElements[i,j] = Player = Instantiate(PlayerPrefab, new Vector3(j - MapWidth / 2, i - MapHeight / 2, 0), Quaternion.identity);
                        player_v = i;
                        player_h = j;
                        Map[player_v, player_h] = player;
                        IsPlayerInstantiated = true;
                    }
                }
            }
        }

        private void SpawnBoss()
        {
                while (!IsBossInstantiated)
                {
                    int i = Random.Range(2, MapWidth - 2);
                    int j = Random.Range(2, MapHeight - 2);
                    if (Map[i, j] == road && Map[i + 1, j] == road && Map[i - 1, j] == road && Map[i, j + 1] == road && Map[i, j - 1] == road)
                    {
                        DungeonElements[i, j] = Boss = Instantiate(BossPrefab, new Vector3(j - MapWidth / 2, i - MapHeight / 2, 0), Quaternion.identity);
                        Map[i,j] = boss;
                        IsBossInstantiated = true;
                    }
                }
        }

        private void CheckInput()
        {
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if(Map[player_v,player_h - 1] != wall)
                {
                    RandomWalk();
                    Player.transform.position = new Vector3(player_h - 1 - MapWidth / 2, player_v - MapHeight / 2, 0);
                    if(Map[player_v, player_h - 1] != road)
                    {
                        Destroy(DungeonElements[player_v, player_h - 1]);
                        CheckDungeonMass(Map[player_v, player_h - 1]);
                    }
                    Map[player_v, player_h - 1] = player;
                    Map[player_v, player_h] = road;
                    player_h -= 1;
                }
            }
            else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (Map[player_v, player_h + 1] != wall)
                {
                    RandomWalk();
                    Player.transform.position = new Vector3(player_h + 1 - MapWidth / 2, player_v - MapHeight / 2, 0);
                    if (Map[player_v, player_h + 1] != road)
                    {
                        Destroy(DungeonElements[player_v, player_h + 1]);
                        CheckDungeonMass(Map[player_v, player_h + 1]);
                    }
                    Map[player_v, player_h + 1] = player;
                    Map[player_v, player_h] = road;
                    player_h += 1;
                }
            }

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (Map[player_v + 1, player_h] != wall)
                {
                    RandomWalk();
                    Player.transform.position = new Vector3(player_h - MapWidth / 2, player_v + 1 - MapHeight / 2, 0);
                    if (Map[player_v + 1, player_h] != road)
                    {
                        Destroy(DungeonElements[player_v + 1, player_h]);
                        CheckDungeonMass(Map[player_v + 1, player_h]);
                    }
                    Map[player_v + 1, player_h] = player;
                    Map[player_v, player_h] = road;
                    player_v += 1;
                }
            }
            else if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (Map[player_v - 1, player_h] != wall)
                {
                    RandomWalk();
                    Player.transform.position = new Vector3(player_h - MapWidth / 2, player_v - 1 - MapHeight / 2, 0);
                    if (Map[player_v - 1, player_h] != road)
                    {
                        Destroy(DungeonElements[player_v - 1, player_h]);
                        CheckDungeonMass(Map[player_v - 1, player_h]);
                    }
                    Map[player_v - 1, player_h] = player;
                    Map[player_v, player_h] = road;
                    player_v -= 1;
                }
            }

        }

        private void RandomWalk()
        {
            for(int i = 0;i<MapWidth; i++)
            {
                for(int j = 0;j<MapHeight; j++)
                {
                    int rand = Random.Range(1, 10);
                    if (Map[i, j] == enemy)
                    {
                        if (DungeonElements[i, j] != null)
                        {
                            switch (rand)
                            {
                                case 1:
                                    if (Map[i + 1, j] == road)
                                    {
                                        Map[i, j] = road;
                                        Map[i + 1, j] = enemy;
                                        DungeonElements[i, j].transform.position = new Vector3(j - MapWidth / 2, i + 1 - MapHeight / 2, 0);
                                        var obj = DungeonElements[i, j];
                                        DungeonElements[i, j] = DungeonElements[i + 1, j];
                                        DungeonElements[i + 1, j] = obj;
                                        i++;
                                    }
                                    break;
                                case 2:
                                    if (Map[i - 1, j] == road)
                                    {
                                        Map[i, j] = road;
                                        Map[i - 1, j] = enemy;
                                        DungeonElements[i, j].transform.position = new Vector3(j - MapWidth / 2, i - 1 - MapHeight / 2, 0);
                                        var obj = DungeonElements[i, j];
                                        DungeonElements[i, j] = DungeonElements[i - 1, j];
                                        DungeonElements[i - 1, j] = obj;
                                    }
                                    break;
                                case 3:
                                    if (Map[i, j + 1] == road)
                                    {
                                        Map[i, j] = road;
                                        Map[i, j + 1] = enemy;
                                        DungeonElements[i, j].transform.position = new Vector3(j + 1 - MapWidth / 2, i - MapHeight / 2, 0);
                                        var obj = DungeonElements[i, j];
                                        DungeonElements[i, j] = DungeonElements[i, j+1];
                                        DungeonElements[i, j+1] = obj;
                                        j++;
                                    }
                                    break;
                                case 4:
                                    if (Map[i, j - 1] == road)
                                    {
                                        Map[i, j] = road;
                                        Map[i, j - 1] = enemy;
                                        DungeonElements[i, j].transform.position = new Vector3(j - 1 - MapWidth / 2, i - MapHeight / 2, 0);
                                        var obj = DungeonElements[i, j];
                                        DungeonElements[i, j] = DungeonElements[i, j-1];
                                        DungeonElements[i, j-1] = obj;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }


        private void CheckDungeonMass(int mass)
        {
            switch (mass)
            {
                case enemy:
                    Enemy.Instance.IsBoss = false;
                    GameController.Instance.ChangeState(GameController.State.BattleStart);
                    break;
                case boss:
                    Enemy.Instance.IsBoss = true;
                    GameController.Instance.ChangeState(GameController.State.BattleStart);
                    break;
                case buildmass:
                    GameController.Instance.ChangeState(GameController.State.Card);
                    break;
                case eventmass:
                    GameController.Instance.ChangeState(GameController.State.Event);
                    break;
            }
        }

    }
}