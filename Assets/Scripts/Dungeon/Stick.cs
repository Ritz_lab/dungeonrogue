﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;
public class Stick : MonoBehaviour
{
    public Button Button;

    public bool IsPush;

    private Vector3 MousePosition;

    private Vector3 MousePositionInWorld;

    private Vector3 ButtonPosition;

    public GameObject Player;

    float MoveSpeed = 0.03f;

    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        ButtonPosition = Button.transform.position;
        Debug.Log(Button.transform.position);
        Debug.Log(Button.transform.localPosition);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.Instance.CurrentState == GameController.State.InDungeon)
        {
            if(Player == null)
            {
                Player = GameObject.FindGameObjectWithTag("Player");
                //animator = Player.GetComponent<Animator>();
            }
            ButtonPosition = transform.parent.position;
            if (IsPush)
            {
                
                MousePosition = Input.mousePosition;
                MousePositionInWorld = Camera.main.ScreenToWorldPoint(MousePosition);
                float f = GetRad(ButtonPosition, MousePositionInWorld);
                float dis = Vector2.Distance(ButtonPosition, MousePositionInWorld);
                Button.GetComponent<Image>().color = Color.grey;
                if (dis <= 0.7f)
                {
                    Button.transform.position = new Vector3(ButtonPosition.x + Mathf.Cos(f) * dis, ButtonPosition.y + Mathf.Sin(f) * dis, 0);
                    Player.transform.position = new Vector3(Player.transform.position.x + Mathf.Cos(f) * dis * MoveSpeed, Player.transform.position.y + Mathf.Sin(f) * dis * MoveSpeed, 0);
                }
                else
                {
                    Button.transform.position = new Vector3(ButtonPosition.x + Mathf.Cos(f) * 0.7f, ButtonPosition.y + Mathf.Sin(f) * 0.7f, 0);
                    Player.transform.position = new Vector3(Player.transform.position.x + Mathf.Cos(f) * MoveSpeed, Player.transform.position.y + Mathf.Sin(f) * MoveSpeed, 0);
                }
                /*
                animator.speed = 1.0f;
                animator.SetFloat("x", Mathf.Cos(f));
                animator.SetFloat("y", Mathf.Sin(f));
                */
            }
            else
            {
                Button.transform.position = ButtonPosition;
                Button.GetComponent<Image>().color = Color.white;
            }
        }
    }

    public void OnDisable()
    {
        Button.transform.position = ButtonPosition;
        IsPush = false;
    }
    public void PushUp()
    {
        IsPush = false;
    }

    public void PushDown()
    {
        IsPush = true;
    }

    public float GetRad(Vector3 start, Vector3 end)
    {
        Vector3 sa = end - start;
        float rad = Mathf.Atan2(sa.y, sa.x);
        return rad;
    }
}
