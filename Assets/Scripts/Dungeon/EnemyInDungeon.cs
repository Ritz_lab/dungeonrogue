﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Manager
{
    public class EnemyInDungeon : MonoBehaviour
    {
        public GameObject Player;

        public int Direction;

        public float MoveSpeed;

        public Animator EnemyAnimator;

        private void Start()
        {
            MoveSpeed = 0.01f;
        }
        // Update is called once per frame
        void Update()
        {
            if(GameController.Instance.CurrentState == GameController.State.InDungeon)
            {
                if (DungeonManager.Instance.PlayerStick.IsPush)
                {
                        int rand = Random.Range(0, 200);
                        if (rand == 0)
                        {
                            Direction = RandomWalk();
                        }
                        switch (Direction)
                        {
                            case 0:
                                transform.position = new Vector3(transform.position.x + MoveSpeed, transform.position.y, 0);
                                EnemyAnimator.SetFloat("MoveHorizontal", 0.1f);
                                break;
                            case 1:
                                transform.position = new Vector3(transform.position.x - MoveSpeed, transform.position.y, 0);
                                EnemyAnimator.SetFloat("MoveHorizontal", -0.1f);
                                break;
                            case 2:
                                transform.position = new Vector3(transform.position.x, transform.position.y + MoveSpeed, 0);
                                EnemyAnimator.SetFloat("MoveVertical", 0.1f);
                                break;
                            case 3:
                                transform.position = new Vector3(transform.position.x, transform.position.y - MoveSpeed, 0);
                                EnemyAnimator.SetFloat("MoveVertical", -0.1f);
                                break;
                            default:
                                EnemyAnimator.SetFloat("MoveHorizontal", 0.1f);
                                break;
                        }
                }
            }
        }

        public int RandomWalk()
        {
            int rand = Random.Range(0, 10);
            return rand;
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "EnemyInDungeon" || collision.gameObject.tag == "Boss" || collision.gameObject.tag == "Event" || collision.gameObject.tag == "Build")
            {
                Direction = RandomWalk();
            }
        }
    }
}
