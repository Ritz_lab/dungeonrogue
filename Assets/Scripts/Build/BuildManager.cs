﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardState;
using Manager;

public class BuildManager : MonoBehaviour
{
    public static BuildManager Instance;

    public int CardNumber;

    public enum State
    {
        BuildStart,
        PowerUp,
        GetEquipment,
        GetSkill,
        Event,
        BuildEnd
    }

    public State CurrentState;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.Instance.CurrentState == GameManager.State.Build)
        {
            UpdateState();
        }
    }

    public void ChangeState(State state)
    {
        CurrentState = state;
    }

    public void SwitchState(Card.State state)
    {

      switch (state)
         {
            case Card.State.Attack:
                ChangeState(State.PowerUp);
                break;
            case Card.State.Defense:
                ChangeState(State.PowerUp);
                break;
            case Card.State.HP:
                ChangeState(State.PowerUp);
                break;
            case Card.State.Armor:
                ChangeState(State.GetEquipment);
                break;
            case Card.State.Boots:
                ChangeState(State.GetEquipment);
                break;
            case Card.State.Gauntlet:
                ChangeState(State.GetEquipment);
                break;
            case Card.State.Helmet:
                ChangeState(State.GetEquipment);
                break;
            case Card.State.Weapon:
                ChangeState(State.GetEquipment);
                break;
            case Card.State.Skill:
                ChangeState(State.GetSkill);
                break;
        }
    }

    public void UpdateState()
    {
        if (CardCreator.Instance.SelectedCards.Count <= 0 || CardCreator.Instance.SelectedCards == null)
        {
            ChangeState(State.BuildEnd);
        }
        switch (CurrentState)
        {
            case State.BuildStart:
                SwitchState(CardCreator.Instance.SelectedCards[CardNumber].GetComponent<Card>().CurrentState);
                break;
            case State.PowerUp:
                break;
            case State.GetEquipment:
                break;
            case State.Event:
                break;
            case State.BuildEnd:
                CardNumber = 0;
                GameManager.Instance.ChangeState(GameManager.State.Battle);
                BattleManager.Instance.ChangeState(BattleManager.State.BattleStart);
                CurrentState = State.BuildStart;
                break;
        }
    }

    public void PlusCardNumber()
    {
        if(CardNumber < CardCreator.Instance.SelectedCards.Count)
        {
            CardNumber++;
        }
    }
}
