﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileTest : MonoBehaviour
{
    public Tilemap Tilemap;
    public TileBase Wall;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                Tilemap.SetTile(new Vector3Int(i, j, 0), Wall);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
