﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Button))]
public class ButtonTextColorSetter : MonoBehaviour,IPointerClickHandler,IPointerEnterHandler,IPointerExitHandler
{
    public Button button;
    public Text text;

    void Start()
    {
        button = GetComponent<Button>();
        text = GetComponentInChildren<Text>();
    }

    private void OnEnable()
    {
        if (button == null)
        {
            button = GetComponent<Button>();
        }
        if (text == null)
        {
            text = GetComponentInChildren<Text>();
        }
        text.color = button.colors.normalColor;
    }

    private void OnDisable()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        text.color = button.colors.pressedColor;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.color = button.colors.highlightedColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.color = button.colors.normalColor;
    }

}
