﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;

namespace BattleState
{
    public class Enemy : Character
    {
        public static Enemy Instance;

        public GameObject EnemyPanelPrefab;

        public GameObject EnemyPanel;

        public Text EnemyNameText;

        public int Level;

        public bool IsBoss;

        public bool IsKilled;

        public EnemyType EnemyType;

        public EnemyType.MobEnemyTypes CurrentMobType;

        public EnemyType.BossEnemyTypes CurrentBossType;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this);
                return;
            }
            Instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            Reset();
        }

        // Update is called once per frame
        async void Update()
        {
            await Battle();
        }

        private async UniTask Battle()
        {
            if (GameController.Instance.CurrentState == GameController.State.BattleStart ||
                GameController.Instance.CurrentState == GameController.State.CheckTurn ||
                GameController.Instance.CurrentState == GameController.State.CheckCondtions ||
                GameController.Instance.CurrentState == GameController.State.EnemyTurn ||
                GameController.Instance.CurrentState == GameController.State.PlayerTurn ||
                GameController.Instance.CurrentState == GameController.State.BattleEnd)
            {
                SkillField.gameObject.SetActive(true);
                checkCurrentHP();
                SetHPBar();
                if (GameController.Instance.CurrentState == GameController.State.BattleStart)
                {
                    HPBar.gameObject.SetActive(true);
                    if (IsBoss)
                    {
                        int bossrand = Random.Range(0, System.Enum.GetNames(typeof(EnemyType.BossEnemyTypes)).Length - 1);
                        CurrentBossType = (EnemyType.BossEnemyTypes)System.Enum.ToObject(typeof(EnemyType.BossEnemyTypes), bossrand);
                        EnemyType.SetBossEnemy(CurrentBossType);
                        MaxHP = (int)(EnemyType.BaseHP * GameController.Instance.CurrentStage * (1f + 0.1f * GameController.Instance.KilledEnemy));
                        Attack = (int)(EnemyType.BaseAttack * GameController.Instance.CurrentStage * (1f + 0.1f * GameController.Instance.KilledEnemy));
                        Defense = (int)(EnemyType.BaseDefense * GameController.Instance.CurrentStage * (1f + 0.1f * GameController.Instance.KilledEnemy));
                        Speed = (int)(EnemyType.BaseSpeed * GameController.Instance.CurrentStage * (1f + 0.1f * GameController.Instance.KilledEnemy));
                        CurrentHP = MaxHP;
                        EnemyPanel.GetComponentsInChildren<Image>()[0].sprite = EnemyType.EnemySprite;
                        EnemyNameText.text = CurrentBossType.ToString();
                        Debug.Log(CurrentBossType);
                    }
                    else
                    {
                        int mobrand = Random.Range(0, System.Enum.GetNames(typeof(EnemyType.MobEnemyTypes)).Length - 1);
                        CurrentMobType = (EnemyType.MobEnemyTypes)System.Enum.ToObject(typeof(EnemyType.MobEnemyTypes), mobrand);
                        EnemyType.SetMobEnemy(CurrentMobType);
                        SetEnemySkills();
                        MaxHP = (int)(EnemyType.BaseHP * GameController.Instance.CurrentStage * (1f + 0.01f * GameController.Instance.KilledEnemy));
                        Attack = (int)(EnemyType.BaseAttack * GameController.Instance.CurrentStage * (1f + 0.01f * GameController.Instance.KilledEnemy));
                        Defense = (int)(EnemyType.BaseDefense * GameController.Instance.CurrentStage * (1f + 0.01f * GameController.Instance.KilledEnemy));
                        Speed = (int)(EnemyType.BaseSpeed * GameController.Instance.CurrentStage * (1f + 0.1f * GameController.Instance.KilledEnemy));
                        CurrentHP = MaxHP;
                        EnemyPanel.GetComponentsInChildren<Image>()[0].sprite = EnemyType.EnemySprite;
                        EnemyNameText.text = CurrentMobType.ToString();
                        Debug.Log(CurrentMobType);
                    }
                    AbnormalConditions["Burn"] = 0;
                    AbnormalConditions["Addiction"] = 0;
                    AbnormalConditions["Petrified"] = 0;
                    for (int i = 0; i < Skills.Count; i++)
                    {
                        Skills[i].GetComponent<Skill>().CoolDown = 0;
                    }
                    GameController.Instance.ChangeState(GameController.State.CheckTurn);
                }
                if (GameController.Instance.CurrentState == GameController.State.EnemyTurn)
                {

                    if (!IsTurnEnd)
                    {
                        int count = 0;
                        for (int i = 0; i < Skills.Count; i++)
                        {
                            if (Skills[i].GetComponent<Skill>().CoolDown > 0)
                            {
                                Skills[i].GetComponent<Skill>().CoolDown--;
                            }
                            if (Skills[i].GetComponent<Skill>().CoolDown == 0)
                            {
                                Skills[i].GetComponent<Button>().interactable = true;
                                count++;
                            }
                        }
                        if (count <= 0)
                        {
                            IsTurnEnd = true;
                            GameController.Instance.ChangeState(GameController.State.CheckTurn);
                        }
                        else
                        {
                            while (!IsTurnEnd)
                            {
                                int rand = Random.Range(0, 5);
                                if (Skills[rand].GetComponent<Skill>().CoolDown == 0)
                                {
                                    IsTurnEnd = true;
                                    await UniTask.Delay(500);
                                    await Skills[rand].GetComponent<Skill>().OnClickSkill();
                                }
                            }
                        }
                        Debug.Log(GameController.Instance.CurrentState);
                    }

                }
                else if(GameController.Instance.CurrentState == GameController.State.CheckCondtions)
                {
                    for (int i = 0; i < Skills.Count; i++)
                    {
                        Skills[i].GetComponent<Button>().interactable = false;
                    }
                    if (GameController.Instance.NextState == GameController.State.EnemyTurn)
                    {
                        if (!IsCheckedConditions)
                        {
                            IsCheckedConditions = true;
                            await CheckAddiction();
                            await CheckBurn();
                            await CheckPetrified();
                            Debug.Log("EnemyCheckCondition");
                            if (CurrentHP > 0 && Player.Instance.CurrentHP > 0)
                            {
                                await UniTask.Delay(500);
                                GameController.Instance.ChangeState(GameController.State.EnemyTurn);
                            }
                            else
                            {
                                await UniTask.Delay(1000);
                                GameController.Instance.ChangeState(GameController.State.CheckTurn);
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < Skills.Count; i++)
                    {
                        Skills[i].GetComponent<Button>().interactable = false;
                    }
                }
                EnemyPanel.SetActive(true);
                EnemyNameText.gameObject.SetActive(true);
            }
        }

        public void SetEnemySkills()
        {
            for(int i = 0; i < 5; i++)
            {
                int rand = Random.Range(0, EnemyType.EnemySkills.Count - 1);
                Skills[i].GetComponent<Skill>().SetSkill(EnemyType.EnemySkills[rand]);
            }
        }

        public void Reset()
        {
            foreach(GameObject obj in Skills)
            {
                Destroy(obj);
            }
            Skills.RemoveAll(obj => SkillPrefab);
            for (int i = 0; i < 5; i++)
            {
                var obj = Instantiate(SkillPrefab, SkillField);
                Skills.Add(obj);
                obj.GetComponent<Skill>().SetRandomSkill();
            }
            if (HPBar == null)
            {
                HPBar = Instantiate(HPBarPrefab, HPBarField);
            }
            HPBar.gameObject.SetActive(true);
            HPBar.GetComponentInChildren<Image>().fillAmount = (float)CurrentHP / (float)MaxHP;
            HPBar.GetComponentInChildren<Text>().text = CurrentHP + " / " + MaxHP;
            AbnormalConditions["Burn"] = 0;
            AbnormalConditions["Addiction"] = 0;
            AbnormalConditions["Petrified"] = 0;
            if (EnemyPanel == null)
            {
                EnemyPanel = Instantiate(EnemyPanelPrefab, ImageField);
            }
        }
    }
}