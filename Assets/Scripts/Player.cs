﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;

namespace BattleState
{
    public class Player : Character
    {
        public static Player Instance;

        public PlayerSetting PlayerSetting;

        public GameObject Equipments, Pool;

        public GameObject SkillPool;

        public GameObject PlayerImage,PlayerImageInstance;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this);
                return;
            }
            Instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            Reset();
        }

        // Update is called once per frame
        async void Update()
        {
            await Battle();
            if (CurrentHP == 0)
            {
                GameController.Instance.ChangeState(GameController.State.GameOver);
            }
            MaxHP = PlayerSetting.BaseHP + PlayerSetting.AddedHP;
            Attack = PlayerSetting.BaseAttack + PlayerSetting.AddedAttack;
            Defense = PlayerSetting.BaseDefense + PlayerSetting.AddedDefense;
            Speed = PlayerSetting.BaseSpeed + PlayerSetting.AddedSpeed;
            if (Equipments.GetComponent<Armor>() != null)
            {
                MaxHP = MaxHP + Equipments.GetComponent<Armor>().HP;
                Attack = Attack + Equipments.GetComponent<Armor>().Attack;
                Defense = Defense + Equipments.GetComponent<Armor>().Defense;
            }
            if (Equipments.GetComponent<Boots>() != null)
            {
                MaxHP = MaxHP + Equipments.GetComponent<Boots>().HP;
                Attack = Attack + Equipments.GetComponent<Boots>().Attack;
                Defense = Defense + Equipments.GetComponent<Boots>().Defense;
            }
            if (Equipments.GetComponent<Gauntlet>() != null)
            {
                MaxHP = MaxHP + Equipments.GetComponent<Gauntlet>().HP;
                Attack = Attack + Equipments.GetComponent<Gauntlet>().Attack;
                Defense = Defense + Equipments.GetComponent<Gauntlet>().Defense;
            }
            if (Equipments.GetComponent<Weapon>() != null)
            {
                MaxHP = MaxHP + Equipments.GetComponent<Weapon>().HP;
                Attack = Attack + Equipments.GetComponent<Weapon>().Attack;
                Defense = Defense + Equipments.GetComponent<Weapon>().Defense;
            }
            if (Equipments.GetComponent<Helmet>() != null)
            {
                MaxHP = MaxHP + Equipments.GetComponent<Helmet>().HP;
                Attack = Attack + Equipments.GetComponent<Helmet>().Attack;
                Defense = Defense + Equipments.GetComponent<Helmet>().Defense;
            }
        }

        private async UniTask Battle() {
            if (GameController.Instance.CurrentState == GameController.State.BattleStart ||
                GameController.Instance.CurrentState == GameController.State.CheckTurn ||
                GameController.Instance.CurrentState == GameController.State.CheckCondtions ||
                GameController.Instance.CurrentState == GameController.State.EnemyTurn ||
                GameController.Instance.CurrentState == GameController.State.PlayerTurn ||
                GameController.Instance.CurrentState == GameController.State.BattleEnd)
            {
                SkillField.gameObject.SetActive(true);
                SetHPBar();
                checkCurrentHP();
                if (GameController.Instance.CurrentState == GameController.State.PlayerTurn)
                {
                    if (!IsTurnEnd)
                    {
                        int count = 0;

                        for (int i = 0; i < Skills.Count; i++)
                        {
                            if (Skills[i].GetComponent<Skill>().CoolDown > 0)
                            {
                                Skills[i].GetComponent<Skill>().CoolDown--;
                            }
                            if (Skills[i].GetComponent<Skill>().CoolDown == 0)
                            {
                                Skills[i].GetComponent<Button>().interactable = true;
                                count++;
                            }
                        }
                        if (count == 0)
                        {
                            IsTurnEnd = true;
                            GameController.Instance.ChangeState(GameController.State.CheckTurn);
                        }
                        IsTurnEnd = true;
                    }
                    else
                    {
                        int count = 0;
                        for (int i = 0; i < Skills.Count; i++)
                        {
                            if (Skills[i].GetComponent<Skill>().CoolDown == 0)
                            {
                                count++;
                            }
                        }
                        if (count == 0)
                        {
                            GameController.Instance.ChangeState(GameController.State.CheckTurn);
                        }
                    }
                }
                else if(GameController.Instance.CurrentState == GameController.State.CheckCondtions)
                {
                    for (int i = 0; i < Skills.Count; i++)
                    {
                        Skills[i].GetComponent<Button>().interactable = false;
                    }
                    if (GameController.Instance.NextState == GameController.State.PlayerTurn)
                    {
                        if (!IsCheckedConditions)
                        {
                            IsCheckedConditions = true;
                            await CheckAddiction();
                            await CheckBurn();
                            await CheckPetrified();
                            Debug.Log("PlayerCheckCondition");
                        }
                        else if (IsCheckedConditions)
                        {
                            if (CurrentHP > 0 && Enemy.Instance.CurrentHP > 0)
                            {
                                await UniTask.Delay(1000);
                                GameController.Instance.ChangeState(GameController.State.PlayerTurn);
                            }
                            else
                            {
                                await UniTask.Delay(1000);
                                GameController.Instance.ChangeState(GameController.State.CheckTurn);
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < Skills.Count; i++)
                    {
                        Skills[i].GetComponent<Button>().interactable = false;
                    }
                }
            }
        }
        public void Reset()
        {
            foreach (GameObject obj in Skills)
            {
                Destroy(obj);
            }
            Skills.RemoveAll(obj => SkillPrefab);
            IsTurnEnd = false;
            PlayerSetting.AddedHP = 0;
            PlayerSetting.AddedAttack = 0;
            PlayerSetting.AddedDefense = 0;
            MaxHP = PlayerSetting.BaseHP;
            Attack = PlayerSetting.BaseAttack;
            Defense = PlayerSetting.BaseDefense;
            CurrentHP = MaxHP;
            for (int i = 0; i < 5; i++)
            {
                var obj = Instantiate(SkillPrefab, SkillField);
                Skills.Add(obj);
            }
            Skills[0].GetComponent<Skill>().SetSkill(Skill.SkillName.Petrify);
            Skills[1].GetComponent<Skill>().SetSkill(Skill.SkillName.FlameSlash);
            Skills[2].GetComponent<Skill>().SetSkill(Skill.SkillName.Heal);
            Skills[3].GetComponent<Skill>().SetSkill(Skill.SkillName.Bash);
            Skills[4].GetComponent<Skill>().SetSkill(Skill.SkillName.Punch);
            SkillPool = Instantiate(SkillPrefab, SkillPool.transform);
            SkillPool.SetActive(false);
            if (HPBar == null)
            {
                HPBar = Instantiate(HPBarPrefab, HPBarField);
            }
            if (PlayerImageInstance == null)
            {
                PlayerImageInstance = Instantiate(PlayerImage, ImageField);
            }
            Equipments.GetComponent<Armor>().HP = 0;
            Equipments.GetComponent<Armor>().Attack = 0;
            Equipments.GetComponent<Armor>().Defense = 0;
            Equipments.GetComponent<Boots>().HP = 0;
            Equipments.GetComponent<Boots>().Attack = 0;
            Equipments.GetComponent<Boots>().Defense = 0;
            Equipments.GetComponent<Gauntlet>().HP = 0;
            Equipments.GetComponent<Gauntlet>().Attack = 0;
            Equipments.GetComponent<Gauntlet>().Defense = 0;
            Equipments.GetComponent<Helmet>().HP = 0;
            Equipments.GetComponent<Helmet>().Attack = 0;
            Equipments.GetComponent<Helmet>().Defense = 0;
            Equipments.GetComponent<Weapon>().HP = 0;
            Equipments.GetComponent<Weapon>().Attack = 0;
            Equipments.GetComponent<Weapon>().Defense = 0;
            AbnormalConditions["Burn"] = 0;
            AbnormalConditions["Addiction"] = 0;
            AbnormalConditions["Petrified"] = 0;
        }
    }
}
