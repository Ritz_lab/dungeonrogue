﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ImageSetting", menuName = "ScriptableObjects/ImageSetting")]
public class ImageSetting:ScriptableObject
{
    public Sprite Attack, Defense, HP,Speed, Helmet, Armor, Gauntlet, Boots,Weapon, Event, Skill, Ability;

    public Sprite Punch, Kick, Jab, Bash, Guard, Heal, Bind,Slash,Sting,Swing;

    public Sprite Ammonite,Arachne,BlackDragon, Brain,Cake,CrazyDolphin,DandyCrescent,DarkHorse,Dice1,Dice2,Dice3,Dice4,Dice5,Dice6,Dice7,
        Dolphin,Dragon,DragonKid, Druid,Faceless,Fighter, Heart,Lizard,Mashroom,MegaWhale,Orochi, Pegasus,Penguin,Phoenix, RedDaragon,
        Slime,Snake,Sun,Tsuchinoko,Unicorn,Villager,Wisp, Wyvern;
}
