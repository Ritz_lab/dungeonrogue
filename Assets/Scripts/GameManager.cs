﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        public PlayerSetting playerSetting;

        public Canvas CardCanvas, BuildCanvas, BattleCanvas;

        public Text text;

        public enum State
        {
            Prepare,
            Start,
            Card,
            Build,
            Battle,
            StageUp,
            GameEnd
        }

        public State CurrentState;

        public int CurrentStage;

        public int KilledEnemy;

        public Text FloorText;

        private void Awake()
        {
            if (Instance != null)
            {
                GameObject.Destroy(this);
                return;
            }
            Instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            CurrentStage = 1;
            KilledEnemy = 0;
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log(CurrentState);
            if (CurrentState == State.GameEnd)
            {
                text.text = "You Lose";
                text.gameObject.SetActive(true);
            }
            FloorText.text = "Floor " + CurrentStage;
        }

        public void ChangeState(State state)
        {
            CurrentState = state;
        }

        public void SwitchCanvas()
        {
            switch (CurrentState)
            {
                case State.Card:
                    break;
                case State.Build:
                    break;
                case State.Battle:
                    break;
            }
        }

        public void Stageup()
        {
            CurrentStage++;
        }

        public void KilledEnemyPlus()
        {
            KilledEnemy++;
        }
    }
