﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;
using BattleState;
using CardState;
public class Event : MonoBehaviour
{

    public Text SituationText, ResultText;
    public Button PositiveButton, NeutralButton, NegativeButton;


    public enum EventState
    {
        GetDamage,
        GetHeal,
        GetBurn,
        GetAddiction,
        GetAttack,
        GetDefense,
        GetHP,
        GetSpeed,
        GetPetrified,
        GetCardCost,
        GetCardChoice,
        GetSkillLevel,
    }

    public EventState CurrentState;

    public int TempStatus;
    // Start is called before the first frame update
    void Start()
    {
        PositiveButton.onClick.AddListener(() => OnClickPositive());
        NeutralButton.onClick.AddListener(() => OnClickNeutral());
        NegativeButton.onClick.AddListener(() => OnClickNegative());
    }

    // Update is called once per frame
    void Update()
    {
        PositiveButton.GetComponentInChildren<Text>().text = "うけいれる";
        NeutralButton.GetComponentInChildren<Text>().text = "すすむ";
        NegativeButton.GetComponentInChildren<Text>().text = "きょひ";
    }

    private void OnEnable()
    {
        SetCurrentState();
        SetSituationText();
    }

    public void SetCurrentState()
    {
        int rand = Random.Range(0, System.Enum.GetNames(typeof(EventState)).Length - 1);
        CurrentState = (EventState)System.Enum.ToObject(typeof(EventState), rand);
        PositiveButton.gameObject.SetActive(true);
        NeutralButton.gameObject.SetActive(false);
        NegativeButton.gameObject.SetActive(true);
    }

    public void SetSituationText()
    {
        ResultText.gameObject.SetActive(false);
        SituationText.gameObject.SetActive(true);
        SituationText.text = "うらないしがあなたにちからをさずけるといってきた。";
    }

    public void SetResultText()
    {
        switch (CurrentState)
        {
            case EventState.GetDamage:
                ResultText.text = "HPがさんぶんのいちになった";
                break;
            case EventState.GetBurn:
                ResultText.text = "やけどになった";
                break;
            case EventState.GetAddiction:
                ResultText.text = "ちゅうどくになった";
                break;
            case EventState.GetPetrified:
                ResultText.text = "せきかがしんこうした";
                break;
            case EventState.GetAttack:
                ResultText.text = "Attackが" + TempStatus + "あがった。";
                break;
            case EventState.GetDefense:
                ResultText.text = "Defenseが" + TempStatus + "あがった。";
                break;
            case EventState.GetHP:
                ResultText.text = "HPが" + TempStatus + "あがった。";
                break;
            case EventState.GetSpeed:
                ResultText.text = "Speedが" + TempStatus + "あがった。";
                break;
            case EventState.GetHeal:
                ResultText.text = "HPがかいふくした";
                break;
            case EventState.GetCardCost:
                ResultText.text = "カードのコストがふえた";
                break;
            case EventState.GetCardChoice:
                ResultText.text = "カードのせんたくしがふえた";
                break;
            case EventState.GetSkillLevel:
                ResultText.text = "スキルレベルがあがった";
                break;
        }
    }


    public void OnClickNeutral()
    {
        GameController.Instance.EventCanvas.gameObject.SetActive(false);

        SoundEffectSetting.Instance.PlaySoundEffect(9);
        GameController.Instance.ChangeState(GameController.State.InDungeon);
    }

    public void OnClickPositive()
    {
        switch (CurrentState)
        {
            case EventState.GetDamage:
                Player.Instance.CurrentHP /= 3 ;
                break;
            case EventState.GetHeal:
                Player.Instance.CurrentHP = Player.Instance.MaxHP;
                break;
            case EventState.GetAttack:
                TempStatus = Random.Range(3, 10);
                Player.Instance.Attack += TempStatus;
                break;
            case EventState.GetDefense:
                TempStatus = Random.Range(5, 10);
                break;
            case EventState.GetHP:
                TempStatus = Random.Range(10, 50);
                Player.Instance.MaxHP += TempStatus;
                break;
            case EventState.GetSpeed:
                TempStatus = Random.Range(1, 5);
                Player.Instance.Speed += TempStatus;
                break;
            case EventState.GetBurn:
                Player.Instance.AbnormalConditions["Burn"] += 3;
                break;
            case EventState.GetAddiction:
                Player.Instance.AbnormalConditions["Addiction"] += 3;
                break;
            case EventState.GetPetrified:
                Player.Instance.AbnormalConditions["Petrified"] += 50;
                break;
            case EventState.GetCardCost:
                CardCreator.Instance.MaxCost++;
                break;
            case EventState.GetCardChoice:
                CardCreator.Instance.CardCount += 3;
                break;
            case EventState.GetSkillLevel:
                for (int i = 0; i < 5; i++)
                {
                    Player.Instance.Skills[i].GetComponent<Skill>().SkillLevel++;
                }
                break;
        }
        PositiveButton.gameObject.SetActive(false);
        NegativeButton.gameObject.SetActive(false);
        SituationText.gameObject.SetActive(false);
        ResultText.gameObject.SetActive(true);
        NeutralButton.gameObject.SetActive(true);

        SetResultText();

        SoundEffectSetting.Instance.PlaySoundEffect(9);
    }

    public void OnClickNegative()
    {
        GameController.Instance.ChangeState(GameController.State.InDungeon);
        GameController.Instance.EventCanvas.gameObject.SetActive(false);

        SoundEffectSetting.Instance.PlaySoundEffect(9);

    }

}
