﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Manager;
using BattleState;
[RequireComponent(typeof(AudioSource))]
public class AudioSetting : MonoBehaviour
{
    public AudioClip[] clips;
    private AudioSource source;
    public AudioMixer mixer;
    float pitch;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        SetSound();
    }

    private void SetSound()
    {
        switch (GameController.Instance.CurrentState)
        {
            case GameController.State.GameStart:
                SetClip(0);
                SetVolume(0.5f);
                Reset();
                source.loop = true;
                break;
            case GameController.State.BattleStart:
                SetEnemySound(Enemy.Instance.IsBoss);
                SetVolume(0.75f);
                break;
            case GameController.State.PlayerTurn:
                PitchBalancer();
                break;
            case GameController.State.CheckCondtions:
                PitchBalancer();
                break;
            case GameController.State.CheckTurn:
                PitchBalancer();
                break;
            case GameController.State.EnemyTurn:
                PitchBalancer();
                break;
            case GameController.State.BattleEnd:
                source.Stop();
                break;
            case GameController.State.InDungeon:
                SetClip(3);
                SetVolume(0.4f);
                Reset();
                break;
            case GameController.State.Pause:
                source.Stop();
                break;
            case GameController.State.Event:
                SetClip(4);
                SetVolume(0.2f);
                Reset();
                break;
            case GameController.State.GameOver:
                source.loop = false;
                SetClip(5);
                SetVolume(0.7f);
                Reset();
                break;
        }
    }

    private void PitchBalancer()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        pitch = (1f - (float)Player.Instance.CurrentHP / (float)Player.Instance.MaxHP) / 2f;
        source.pitch = 1f + pitch;
        pitch = 1f / source.pitch;
        mixer.SetFloat("PitchShifter", pitch);
#endif
    }

    private void SetClip(int num)
    {
        if(!source.isPlaying || source.clip != clips[num])
        {
            source.clip = clips[num];
            source.Play();
        }
    }

    private void SetEnemySound(bool IsBoss)
    {
        if (!IsBoss)
        {
            SetClip(1);
        }
        else
        {
            SetClip(2);
        }
    }

    private void SetVolume(float vol)
    {
        source.volume = vol;
    }

    public void Reset()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        if (source.pitch != 1f)
        {
            source.pitch = 1f;
            mixer.SetFloat("PitchShifter", 1f);
        }
#endif
    }
}
