﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;

public class GameStart : MonoBehaviour
{
    public Button StartButton,ResourcesButton,BackButton;

    private string EnemyResourceURL, UIResourceURL, SoundResourceURL,TilemapResourceURL,FontResourceURL;

    public Text EnemyResourceDescription, UIResourceDescription,SoundResourceDescription,TilemapResourceDescription,FontResourceDescription;

    public Button EnemyResourceButton, UIResourceButton, SoundResourceButton,TilemapResourceButton,FontResourceButton;

    // Start is called before the first frame update
    void Start()
    {
        EnemyResourceURL = "https://lud.sakura.ne.jp/";
        UIResourceURL = "https://junkpixel.com/";
        SoundResourceURL = "https://maou.audio/";
        TilemapResourceURL = "http://zioru.x0.to/";
        FontResourceURL = "https://fonts.google.com/noto/specimen/Noto+Sans+JP";

        EnemyResourceDescription.text = "著者 : qut 様";
        UIResourceDescription.text = "著者 : Kiti 様" + "\n\r" + "サイト : JunkPixel 様";
        SoundResourceDescription.text = "サイト : 魔王魂 様";
        TilemapResourceDescription.text = "製作者 : からし 様" + "\n\r" + "サイト : Un Almacen 様";
        FontResourceDescription.text = "フォント";

        EnemyResourceButton.GetComponentInChildren<Text>().text = EnemyResourceURL;
        UIResourceButton.GetComponentInChildren<Text>().text = UIResourceURL;
        SoundResourceButton.GetComponentInChildren<Text>().text = SoundResourceURL;
        TilemapResourceButton.GetComponentInChildren<Text>().text = TilemapResourceURL;
        FontResourceButton.GetComponentInChildren<Text>().text = FontResourceURL;

        StartButton.onClick.AddListener(() => OnClickStartButton());
        ResourcesButton.onClick.AddListener(() => OnClickResourcesButton());
        BackButton.onClick.AddListener(() => OnClickBackButton());
        EnemyResourceButton.onClick.AddListener(() => OnClickURLButton(EnemyResourceURL));
        UIResourceButton.onClick.AddListener(() => OnClickURLButton(UIResourceURL));
        SoundResourceButton.onClick.AddListener(() => OnClickURLButton(SoundResourceURL));
        TilemapResourceButton.onClick.AddListener(() => OnClickURLButton(TilemapResourceURL));
        FontResourceButton.onClick.AddListener(() => OnClickURLButton(FontResourceURL));

        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize()
    {
        EnemyResourceButton.gameObject.SetActive(false);
        EnemyResourceDescription.gameObject.SetActive(false);
        UIResourceButton.gameObject.SetActive(false);
        UIResourceDescription.gameObject.SetActive(false);
        SoundResourceButton.gameObject.SetActive(false);
        SoundResourceDescription.gameObject.SetActive(false);
        TilemapResourceButton.gameObject.SetActive(false);
        TilemapResourceDescription.gameObject.SetActive(false);
        FontResourceButton.gameObject.SetActive(false);
        FontResourceDescription.gameObject.SetActive(false);

        StartButton.gameObject.SetActive(true);
        ResourcesButton.gameObject.SetActive(true);
        BackButton.gameObject.SetActive(false);
    }

    public void OnClickStartButton()
    {
        SoundEffectSetting.Instance.PlaySoundEffect(9);
        GameController.Instance.CurrentStage = 0;
        GameController.Instance.ChangeState(GameController.State.Reset);
    }

    public void OnClickResourcesButton()
    {
        StartButton.gameObject.SetActive(false);
        ResourcesButton.gameObject.SetActive(false);
        BackButton.gameObject.SetActive(true);

        EnemyResourceButton.gameObject.SetActive(true);
        EnemyResourceDescription.gameObject.SetActive(true);
        UIResourceButton.gameObject.SetActive(true);
        UIResourceDescription.gameObject.SetActive(true);
        SoundResourceButton.gameObject.SetActive(true);
        SoundResourceDescription.gameObject.SetActive(true);
        TilemapResourceButton.gameObject.SetActive(true);
        TilemapResourceDescription.gameObject.SetActive(true);
        FontResourceButton.gameObject.SetActive(true);
        FontResourceDescription.gameObject.SetActive(true);


        SoundEffectSetting.Instance.PlaySoundEffect(9);

    }

    public void OnClickBackButton()
    {
        EnemyResourceButton.gameObject.SetActive(false);
        EnemyResourceDescription.gameObject.SetActive(false);
        UIResourceButton.gameObject.SetActive(false);
        UIResourceDescription.gameObject.SetActive(false);
        SoundResourceButton.gameObject.SetActive(false);
        SoundResourceDescription.gameObject.SetActive(false);
        TilemapResourceButton.gameObject.SetActive(false);
        TilemapResourceDescription.gameObject.SetActive(false);
        FontResourceButton.gameObject.SetActive(false);
        FontResourceDescription.gameObject.SetActive(false);

        StartButton.gameObject.SetActive(true);
        ResourcesButton.gameObject.SetActive(true);
        BackButton.gameObject.SetActive(false);

        SoundEffectSetting.Instance.PlaySoundEffect(9);

    }

    public void OnClickURLButton(string url)
    {
        SoundEffectSetting.Instance.PlaySoundEffect(9);

        Application.OpenURL(url);
    }
}
