﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerSetting", menuName = "ScriptableObjects/PlayerSetting")]
public class PlayerSetting : ScriptableObject
{
    public int BaseHP;

    public int AddedHP;

    public int PlusHP;

    public int BaseAttack;

    public int AddedAttack;

    public int PlusAttack;

    public int BaseDefense;

    public int AddedDefense;

    public int PlusDefense;

    public int BaseSpeed;

    public int AddedSpeed;

    public int PlusSpeed;


}
