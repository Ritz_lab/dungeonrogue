﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : MonoBehaviour
{
    public int Attack, Defense, HP;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //装備の数値を決める
    public void SetStatusRange(int AttackMin,int AttackMax,int DefenseMin,int DefenseMax,int HPMin,int HPMax)
    {
        Attack = Random.Range(AttackMin, AttackMax);
        Defense = Random.Range(DefenseMin, DefenseMax);
        HP = Random.Range(HPMin, HPMax);
    }
    public void SetStatus(int e_Attack,int e_Defense,int e_HP)
    {
        Attack = e_Attack;
        Defense = e_Defense;
        HP = e_HP;
    }
}
