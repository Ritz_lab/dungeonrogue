﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    public static BattleManager Instance;
    public int MaxTurn;
    public int CurrentTurn
    {
        get; private set;
    }
    public enum State
    {   
        BattleStart,
        PlayerTurn,
        EnemyTurn,
        BattleEnd
    }

    public State CurrentState;

    public bool IsFighting;

    private void Awake()
    {
        if(Instance != null)
        {
            GameObject.Destroy(this);
            return;
        }
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.Instance.CurrentState == GameManager.State.Battle)
        {
            UpdateState();
        }
    }

    private void OnEnable()
    {
        ChangeState(State.BattleStart);
    }

    public void ChangeState(State state)
    {
        CurrentState = state;
    }

    private void UpdateState()
    {
        switch (CurrentState) {
            case State.BattleStart:
                //ChangeState(State.PlayerTurn);
                break;
            case State.PlayerTurn:
                break;
            case State.EnemyTurn:
                break;
            case State.BattleEnd:
                CurrentState = State.BattleStart;
                //GameManager.Instance.ChangeState(GameManager.State.Card);
                break;
        }
    }

    public IEnumerator ChangeStateCoroutine(State state)
    {
        yield return new WaitForSeconds(1f);
        ChangeState(state);
        yield return null;
    }
}
