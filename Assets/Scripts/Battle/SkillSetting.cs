﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillSetting", menuName = "ScriptableObjects/SkillSetting")]
public class SkillSetting : ScriptableObject
{
    public enum Type{
        Attack,Defense,Buff,Debuff
    }
    public float Constant;
    public Type type;
    public enum State
    {
        Punch,
        Kick,
        Jab,
        Bash,
        Guard,
        Dodge
    }

}
