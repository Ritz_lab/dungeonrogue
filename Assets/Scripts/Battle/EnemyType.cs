﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BattleState;

[CreateAssetMenu(fileName = "EnemyType", menuName = "ScriptableObjects/EnemyType")]
public class EnemyType : ScriptableObject
{
    public ImageSetting ImageSetting;

    public enum MobEnemyTypes
    {
        Ammonite,
        Arachne,
        Cake,
        DandyCrescent,
        DarkHorse,
        Dice1,
        Dice2,
        Dice3,
        Dice4,
        Dice5,
        Dice6,
        Dice7,
        Dolphin,
        DragonKid,
        Druid,
        Faceless,
        Lizard,
        Mashroom,
        MegaWhale,
        Pegasus,
        Penguin,
        Slime,
        Snake,
        TheSun,
        Tsuchinoko,
        Unicorn,
        Villager,
        Wisp,
    }
    public enum BossEnemyTypes
    {
        BlackDragon,
        Brain,
        CrazyDolphin,
        Dragon,
        Fighter,
        Heart,
        Orochi,
        Phoenix,
        RedDaragon,
        Wyvern
    }

    public int BaseHP;

    public int BaseAttack;

    public int BaseDefense;

    public int BaseSpeed;

    public Sprite EnemySprite;

    public Color EnemyPanelColor;

    public List<Skill.SkillName> EnemySkills;

    public void SetMobEnemy(MobEnemyTypes CurrentMobEnemyType)
    {
        EnemySkills.Clear();
        switch (CurrentMobEnemyType)
        {
            case MobEnemyTypes.Ammonite:
                BaseAttack = 3;
                BaseDefense = 15;
                BaseHP = 20;
                BaseSpeed = 1;
                EnemySprite = ImageSetting.Ammonite;
                EnemySkills.Add(Skill.SkillName.Bind);
                EnemySkills.Add(Skill.SkillName.BindSlash);
                EnemySkills.Add(Skill.SkillName.Blizzard);
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.Acid);
                break;
            case MobEnemyTypes.Arachne:
                BaseAttack = 5;
                BaseDefense = 5;
                BaseHP = 15;
                BaseSpeed = 5;
                EnemySprite = ImageSetting.Arachne;
                EnemySkills.Add(Skill.SkillName.Bind);
                EnemySkills.Add(Skill.SkillName.BindSlash);
                EnemySkills.Add(Skill.SkillName.Melt);
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.Acid);
                break;
            case MobEnemyTypes.Cake:
                BaseAttack = 10;
                BaseDefense = 1;
                BaseHP = 50;
                BaseSpeed = 1;
                EnemySprite = ImageSetting.Cake;
                EnemySkills.Add(Skill.SkillName.Jab);
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.FullSwing);
                EnemySkills.Add(Skill.SkillName.Heal);
                break;
            case MobEnemyTypes.DandyCrescent:
                BaseAttack = 1;
                BaseDefense = 30;
                BaseHP = 10;
                BaseSpeed = 1;
                EnemySprite = ImageSetting.DandyCrescent;
                EnemySkills.Add(Skill.SkillName.Heal);
                EnemySkills.Add(Skill.SkillName.Bash);
                EnemySkills.Add(Skill.SkillName.Blast);
                EnemySkills.Add(Skill.SkillName.StoneBreath);
                break;
            case MobEnemyTypes.DarkHorse:
                BaseAttack = 10;
                BaseDefense = 7;
                BaseHP = 25;
                BaseSpeed = 7;
                EnemySprite = ImageSetting.DarkHorse;
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.Bash);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.HighHeal);
                break;
            case MobEnemyTypes.Dice1:
                BaseAttack = 10;
                BaseDefense = 10;
                BaseHP = 10;
                BaseSpeed = 10;
                EnemySprite = ImageSetting.Dice1;
                EnemySkills.Add(Skill.SkillName.Jab);
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.FullSwing);
                EnemySkills.Add(Skill.SkillName.Heal);
                break;
            case MobEnemyTypes.Dice2:
                BaseAttack = 20;
                BaseDefense = 20;
                BaseHP = 20;
                BaseSpeed = 20;
                EnemySprite = ImageSetting.Dice2;
                EnemySkills.Add(Skill.SkillName.Jab);
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.FullSwing);
                EnemySkills.Add(Skill.SkillName.Heal);
                break;
            case MobEnemyTypes.Dice3:
                BaseAttack = 30;
                BaseDefense = 30;
                BaseHP = 30;
                BaseSpeed = 30;
                EnemySprite = ImageSetting.Dice3;
                EnemySkills.Add(Skill.SkillName.Jab);
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.Swing);
                EnemySkills.Add(Skill.SkillName.Heal);
                break;
            case MobEnemyTypes.Dice4:
                BaseAttack = 40;
                BaseDefense = 40;
                BaseHP = 40;
                BaseSpeed = 40;
                EnemySprite = ImageSetting.Dice4;
                EnemySkills.Add(Skill.SkillName.Bash);
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.PowerSwing);
                EnemySkills.Add(Skill.SkillName.HighHeal);
                break;
            case MobEnemyTypes.Dice5:
                BaseAttack = 50;
                BaseDefense = 50;
                BaseHP = 50;
                BaseSpeed = 50;
                EnemySprite = ImageSetting.Dice5;
                EnemySkills.Add(Skill.SkillName.Bash);
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.FullSwing);
                EnemySkills.Add(Skill.SkillName.HighHeal);
                break;
            case MobEnemyTypes.Dice6:
                BaseAttack = 60;
                BaseDefense = 60;
                BaseHP = 60;
                BaseSpeed = 60;
                EnemySprite = ImageSetting.Dice6;
                EnemySkills.Add(Skill.SkillName.Bash);
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.FullSwing);
                EnemySkills.Add(Skill.SkillName.HighHeal);
                break;
            case MobEnemyTypes.Dice7:
                BaseAttack = 70;
                BaseDefense = 70;
                BaseHP = 70;
                BaseSpeed = 70;
                EnemySprite = ImageSetting.Dice7;
                EnemySkills.Add(Skill.SkillName.Jab);
                EnemySkills.Add(Skill.SkillName.Swing);
                EnemySkills.Add(Skill.SkillName.Heal);
                EnemySkills.Add(Skill.SkillName.Bash);
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.PowerSwing);
                EnemySkills.Add(Skill.SkillName.HighHeal);
                break;
            case MobEnemyTypes.Dolphin:
                BaseAttack = 3;
                BaseDefense = 15;
                BaseHP = 20;
                BaseSpeed = 1;
                EnemySprite = ImageSetting.Dolphin;
                break;
            case MobEnemyTypes.DragonKid:
                BaseAttack = 15;
                BaseDefense = 15;
                BaseHP = 30;
                BaseSpeed = 10;
                EnemySprite = ImageSetting.DragonKid;
                EnemySkills.Add(Skill.SkillName.Blast);
                EnemySkills.Add(Skill.SkillName.Blizzard);
                EnemySkills.Add(Skill.SkillName.StoneBreath);
                EnemySkills.Add(Skill.SkillName.FireBreath);
                EnemySkills.Add(Skill.SkillName.Slash);

                break;
            case MobEnemyTypes.Druid:
                BaseAttack = 10;
                BaseDefense = 5;
                BaseHP = 10;
                BaseSpeed = 1;
                EnemySprite = ImageSetting.Druid;
                EnemySkills.Add(Skill.SkillName.Blast);
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.Heal);
                EnemySkills.Add(Skill.SkillName.HighHeal);
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.Bind);
                EnemySkills.Add(Skill.SkillName.BindSlash);
                break;
            case MobEnemyTypes.Faceless:
                BaseAttack = 10;
                BaseDefense = 10;
                BaseHP = 100;
                BaseSpeed = 3;
                EnemySprite = ImageSetting.Faceless;
                EnemySkills.Add(Skill.SkillName.Blast);
                EnemySkills.Add(Skill.SkillName.Blizzard);
                EnemySkills.Add(Skill.SkillName.StoneBreath);
                EnemySkills.Add(Skill.SkillName.FireBreath);
                EnemySkills.Add(Skill.SkillName.Bind);
                EnemySkills.Add(Skill.SkillName.Heal);
                break;
            case MobEnemyTypes.Lizard:
                BaseAttack = 3;
                BaseDefense = 17;
                BaseHP = 20;
                BaseSpeed = 10;
                EnemySprite = ImageSetting.Lizard;
                EnemySkills.Add(Skill.SkillName.Blast);
                EnemySkills.Add(Skill.SkillName.Acid);
                EnemySkills.Add(Skill.SkillName.FireBreath);
                EnemySkills.Add(Skill.SkillName.Bind);
                EnemySkills.Add(Skill.SkillName.Slash);
                break;
            case MobEnemyTypes.Mashroom:
                BaseAttack = 1;
                BaseDefense = 12;
                BaseHP = 20;
                BaseSpeed = 1;
                EnemySprite = ImageSetting.Mashroom;
                EnemySkills.Add(Skill.SkillName.Blast);
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.Bind);
                EnemySkills.Add(Skill.SkillName.Melt);
                EnemySkills.Add(Skill.SkillName.BindSlash);
                break;
            case MobEnemyTypes.MegaWhale:
                BaseAttack = 8;
                BaseDefense = 16;
                BaseHP = 100;
                BaseSpeed = 2;
                EnemySprite = ImageSetting.MegaWhale;
                EnemySkills.Add(Skill.SkillName.Swing);
                EnemySkills.Add(Skill.SkillName.PowerSwing);
                EnemySkills.Add(Skill.SkillName.FullSwing);
                EnemySkills.Add(Skill.SkillName.Heal);
                EnemySkills.Add(Skill.SkillName.Bash);
                break;
            case MobEnemyTypes.Pegasus:
                BaseAttack = 15;
                BaseDefense = 5;
                BaseHP = 40;
                BaseSpeed = 17;
                EnemySprite = ImageSetting.Pegasus;
                EnemySkills.Add(Skill.SkillName.Sting);
                EnemySkills.Add(Skill.SkillName.PowerSting);
                EnemySkills.Add(Skill.SkillName.VampSting);
                EnemySkills.Add(Skill.SkillName.FlameSting);
                EnemySkills.Add(Skill.SkillName.HighHeal);
                break;
            case MobEnemyTypes.Penguin:
                BaseAttack = 1;
                BaseDefense = 1;
                BaseHP = 1;
                BaseSpeed = 1;
                EnemySprite = ImageSetting.Penguin;
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Jab);
                EnemySkills.Add(Skill.SkillName.Heal);
                break;
            case MobEnemyTypes.Slime:
                BaseAttack = 5;
                BaseDefense = 1;
                BaseHP = 200;
                BaseSpeed = 2;
                EnemySprite = ImageSetting.Slime;
                EnemySkills.Add(Skill.SkillName.Acid);
                EnemySkills.Add(Skill.SkillName.Melt);
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.Heal);
                break;
            case MobEnemyTypes.Snake:
                BaseAttack = 8;
                BaseDefense = 4;
                BaseHP = 10;
                BaseSpeed = 6;
                EnemySprite = ImageSetting.Snake;
                EnemySkills.Add(Skill.SkillName.Petrify);
                break;
            case MobEnemyTypes.TheSun:
                BaseAttack = 30;
                BaseDefense = 15;
                BaseHP = 70;
                BaseSpeed = 19;
                EnemySprite = ImageSetting.Sun;
                EnemySkills.Add(Skill.SkillName.FlameSlash);
                EnemySkills.Add(Skill.SkillName.FlameSting);
                EnemySkills.Add(Skill.SkillName.FlameSwing);
                EnemySkills.Add(Skill.SkillName.FireBreath);
                EnemySkills.Add(Skill.SkillName.Bash);
                break;
            case MobEnemyTypes.Tsuchinoko:
                BaseAttack = 4;
                BaseDefense = 4;
                BaseHP = 4;
                BaseSpeed = 100;
                EnemySprite = ImageSetting.Tsuchinoko;
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.StoneBreath);
                break;
            case MobEnemyTypes.Unicorn:
                BaseAttack = 21;
                BaseDefense = 14;
                BaseHP = 35;
                BaseSpeed = 20;
                EnemySprite = ImageSetting.Unicorn;
                EnemySkills.Add(Skill.SkillName.Sting);
                EnemySkills.Add(Skill.SkillName.FlameSting);
                EnemySkills.Add(Skill.SkillName.VampSting);
                EnemySkills.Add(Skill.SkillName.Heal);
                break;
            case MobEnemyTypes.Villager:
                BaseAttack = 5;
                BaseDefense = 5;
                BaseHP = 20;
                BaseSpeed = 5;
                EnemySprite = ImageSetting.Villager;
                EnemySkills.Add(Skill.SkillName.Punch);
                EnemySkills.Add(Skill.SkillName.Kick);
                EnemySkills.Add(Skill.SkillName.Jab);
                EnemySkills.Add(Skill.SkillName.Penetrate);
                break;
            case MobEnemyTypes.Wisp:
                BaseAttack = 20;
                BaseDefense = 6;
                BaseHP = 26;
                BaseSpeed = 10;
                EnemySprite = ImageSetting.Wisp;
                EnemySkills.Add(Skill.SkillName.FireBreath);
                EnemySkills.Add(Skill.SkillName.Blizzard);
                EnemySkills.Add(Skill.SkillName.Bind);
                EnemySkills.Add(Skill.SkillName.Petrify);
                EnemySkills.Add(Skill.SkillName.Melt);
                break;
        }
    }

    public void SetBossEnemy(BossEnemyTypes CurrentBossEnemyType)
    {
        switch (CurrentBossEnemyType)
        {
            case BossEnemyTypes.BlackDragon:
                BaseAttack = 200;
                BaseDefense = 100;
                BaseHP = 550;
                BaseSpeed = 100;
                EnemySprite = ImageSetting.BlackDragon;
                break;
            case BossEnemyTypes.Brain:
                BaseAttack = 150;
                BaseDefense = 25;
                BaseHP = 300;
                BaseSpeed = 200;
                EnemySprite = ImageSetting.Brain;
                break;
            case BossEnemyTypes.CrazyDolphin:
                BaseAttack = 120;
                BaseDefense = 50;
                BaseHP = 400;
                BaseSpeed = 30;
                EnemySprite = ImageSetting.CrazyDolphin;
                break;
            case BossEnemyTypes.Dragon:
                BaseAttack = 130;
                BaseDefense = 70;
                BaseHP = 400;
                BaseSpeed = 80;
                EnemySprite = ImageSetting.Dragon;
                break;
            case BossEnemyTypes.Fighter:
                BaseAttack = 125;
                BaseDefense = 30;
                BaseHP = 350;
                BaseSpeed = 100;
                EnemySprite = ImageSetting.Fighter;
                break;
            case BossEnemyTypes.Heart:
                BaseAttack = 70;
                BaseDefense = 20;
                BaseHP = 600;
                BaseSpeed = 40;
                EnemySprite = ImageSetting.Heart;
                break;
            case BossEnemyTypes.Orochi:
                BaseAttack = 250;
                BaseDefense = 10;
                BaseHP = 750;
                BaseSpeed = 50;
                EnemySprite = ImageSetting.Orochi;
                break;
            case BossEnemyTypes.Phoenix:
                BaseAttack = 30;
                BaseDefense = 1;
                BaseHP = 1000;
                BaseSpeed = 1;
                EnemySprite = ImageSetting.Phoenix;
                break;
            case BossEnemyTypes.RedDaragon:
                BaseAttack = 300;
                BaseDefense = 50;
                BaseHP = 70;
                BaseSpeed = 300;
                EnemySprite = ImageSetting.RedDaragon;
                break;
            case BossEnemyTypes.Wyvern:
                BaseAttack = 200;
                BaseDefense = 35;
                BaseHP = 400;
                BaseSpeed = 150;
                EnemySprite = ImageSetting.Wyvern;
                break;
        }
    }
}
